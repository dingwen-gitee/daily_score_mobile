import 'package:daily_score_mobile/page/schedule/page/table_page.dart';
import 'package:flutter/material.dart';
/// 课程表 page
///Created by dingwen on 2021/4/12.

class SchedulePage extends StatefulWidget {
  @override
  _SchedulePageState createState() => _SchedulePageState();
}

class _SchedulePageState extends State<SchedulePage> {
  @override
  Widget build(BuildContext context) {
    return TablePage();
  }
}
