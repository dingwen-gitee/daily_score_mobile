import 'package:daily_score_mobile/page/schedule/widget/course_widget.dart';
import 'package:flutter/material.dart';

/// 滑动处理
/// @author: dingwen
/// @date: 2021/5/1
class SlideWidget extends StatelessWidget {
  SlideWidget({Key key, this.onWeekChange, this.offset = 1})
      : this.pageController = PageController(initialPage: offset - 1),
        super(key: key);

  final int offset;
  final ValueChanged<int> onWeekChange;
  final PageController pageController;

  void _handlePageChange(int index) async {
    /// 等待到达指定页面时才进行onWeekChange的回调，否者会造成页面的跳动
    /// 指定页面为index，时间为duration，动画为curve
    await pageController.animateToPage(index,
        duration: Duration(milliseconds: 300), curve: Curves.ease);
    onWeekChange(index + 1);
  }

  @override
  Widget build(BuildContext context) {
    print("SlideTable rebuild  $offset");
    final list = <Widget>[];
    for (int i = 1; i <= 20; i++) {
      list.add(CourseTableWidget(
        week: i,
      ));
    }

    return Container(
      child:  PageView(
        /// 这里需要加key，否者flutter无法监测到状态的改变
        key:  Key(offset.toString()),
        children: list,
        controller: pageController,
        onPageChanged: _handlePageChange,
      ),
    );
  }
}
