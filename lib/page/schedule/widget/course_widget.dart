import 'package:flutter/material.dart';

/// TODO
/// @author: dingwen
/// @date: 2021/5/1
class CourseTableWidget extends StatefulWidget {
  CourseTableWidget({Key key, @required this.week}) : super(key: key);

  final int week;

  @override
  _CourseTableWidgetState createState() => _CourseTableWidgetState();
}

class _CourseTableWidgetState extends State<CourseTableWidget> {
  /// 表头宽度  表头高度  格子宽度  格子高度
  double titleWidth;

  double titleHeight;

  double gridWidth;

  double gridHeight;

  Map<String, dynamic> db;

  BoxDecoration titleDecoration;

  BoxDecoration outerGridDecoration;

  BoxDecoration innerGridDecoration;

  Map courseData = {
    "1": {
      "01": {"name": "大学生计算机课程", "room": "博雅楼666", "color": Colors.blue},
      "74": {"name": "大学生计算机课程", "room": "博雅楼666", "color": Colors.blue},
      "104": {"name": "大学生计算机课程", "room": "博雅楼666", "color": Colors.blue},
      "84": {"name": "大学生计算机课程", "room": "博雅楼666", "color": Colors.blue},
      "94": {"name": "大学生计算机课程", "room": "博雅楼666", "color": Colors.blue},
      "03": {"name": "大学生计算机课程", "room": "笃学楼501", "color": Colors.green},
      "16": {"name": "大学生计算机课程", "room": "至善楼413", "color": Colors.deepPurple},
      "10": {"name": "大学生计算机课程", "room": "普教楼202", "color": Colors.orange},
      "22": {"name": "大学生计算机课程", "room": "启智楼102", "color": Colors.orange},
      "21": {"name": "大学生计算机课程", "room": "综合楼303", "color": Colors.orange},
      "36": {"name": "大学生计算机课程", "room": "笃学楼563", "color": Colors.orange},
      "30": {"name": "大学生计算机课程", "room": "笃学楼563", "color": Colors.orange},
      "51": {"name": "大学生计算机课程", "room": "笃学楼563", "color": Colors.red},
      "101": {"name": "大学生计算机课程", "room": "笃学楼563", "color": Colors.red},
      "121": {"name": "大学生计算机课程", "room": "笃学楼563", "color": Colors.red},
      "127": {"name": "大学生计算机课程", "room": "笃学楼563", "color": Colors.red},
    },
    "2": {
      "82": {"name": "大学生计算机课程", "room": "博雅楼666", "color": Colors.blue},
      "71": {"name": "大学生计算机课程", "room": "博雅楼666", "color": Colors.blue},
      "104": {"name": "大学生计算机课程", "room": "博雅楼666", "color": Colors.blue},
      "84": {"name": "大学生计算机课程", "room": "博雅楼666", "color": Colors.blue},
      "90": {"name": "大学生计算机课程", "room": "博雅楼666", "color": Colors.blue},
      "010": {"name": "大学生计算机课程", "room": "笃学楼501", "color": Colors.green},
      "16": {"name": "大学生计算机课程", "room": "至善楼413", "color": Colors.deepPurple},
      "14": {"name": "大学生计算机课程", "room": "普教楼202", "color": Colors.orange},
      "22": {"name": "大学生计算机课程", "room": "启智楼102", "color": Colors.orange},
      "21": {"name": "大学生计算机课程", "room": "综合楼303", "color": Colors.orange},
      "36": {"name": "大学生计算机课程", "room": "笃学楼563", "color": Colors.orange},
      "30": {"name": "大学生计算机课程", "room": "笃学楼563", "color": Colors.orange},
      "51": {"name": "大学生计算机课程", "room": "笃学楼563", "color": Colors.red},
      "101": {"name": "大学生计算机课程", "room": "笃学楼563", "color": Colors.red},
      "121": {"name": "大学生计算机课程", "room": "笃学楼563", "color": Colors.red},
      "127": {"name": "大学生计算机课程", "room": "笃学楼563", "color": Colors.red},
    }
  };

  void _initSize(BuildContext context) {
    print("CourseTable._initSize");
    this.titleWidth = 40.0;
    this.titleHeight = 40.0;
    this.gridWidth = (MediaQuery.of(context).size.width - titleWidth) / 7;
    this.gridHeight = MediaQuery.of(context).size.height / 15.4;

    this.titleDecoration = BoxDecoration(
      color: Colors.grey[100],
    );
    this.outerGridDecoration = BoxDecoration(
      color: Colors.white,
    );
  }

  Widget _renderGrid(BuildContext context, int row, int col) {
    Map data = {};
    if (courseData['${widget.week}'] != null) {
      data = courseData['${widget.week}']["$row$col"];
    }

    final innerWidget = data == null || courseData['${widget.week}'] == null
        ? Container(
            child: null,
          )
        : Container(
            child: Text(
              data["name"] + "@" + data["room"],
              style: TextStyle(
                fontSize: 12.0,
                color: Colors.white,
              ),
            ),
            decoration: BoxDecoration(
              color: data["color"],
              borderRadius: BorderRadius.all(
                const Radius.circular(8.0),
              ),
              boxShadow: <BoxShadow>[
                BoxShadow(
                  color: Colors.grey[300],
                  offset: Offset(1.0, 1.0),
                  blurRadius: 4.0,
                ),
              ],
            ),
            margin: EdgeInsets.all(3.0),
            padding: EdgeInsets.all(2.0),
          );

    return Container(
      child: innerWidget,
      width: gridWidth,
      height: gridHeight,
      decoration: outerGridDecoration,
    );
  }

  Widget _renderTitle(BuildContext context) {
    final weekName = const ["周一", "周二", "周三", "周四", "周五", "周六", "周日"];
    final list = <Widget>[];

    list.add(Container(
      child: null,
      decoration: titleDecoration,
      width: titleWidth,
      height: titleHeight,
    ));

    for (int i = 1; i <= 7; i++) {
      list.add(Container(
        alignment: Alignment.center,
        decoration: titleDecoration,
        child: Text(weekName[i - 1]),
        width: gridWidth,
        height: titleHeight,
      ));
    }

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: list,
    );
  }

  Widget _renderRow(BuildContext context, int row) {
    final list = <Widget>[];

    list.add(Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Text((row + 1).toString()),
        ],
      ),
      decoration: titleDecoration,
      width: titleWidth,
      height: gridHeight,
    ));

    for (int col = 1; col <= 7; col++) {
      list.add(_renderGrid(context, row, col));
    }

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: list,
    );
  }

  @override
  Widget build(BuildContext context) {
    _initSize(context);

    /// 该函数必须首先调用进行宽度高度及数据库的初始化

    final rows = <Widget>[];
    rows.add(_renderTitle(context));
    for (int row = 0; row < 12; row++) {
      rows.add(_renderRow(context, row));
    }

    return Column(
      children: rows,
    );
  }
}
