import 'package:daily_score_mobile/page/schedule/widget/slide_widget.dart';
import 'package:flutter/material.dart';

/// 课程表 table
/// @author: dingwen
/// @date: 2021/5/1
class TablePage extends StatefulWidget {
  TablePage({Key key}) : super(key: key);

  @override
  _TablePageState createState() => _TablePageState();
}

class _TablePageState extends State<TablePage> {
  var _week = 1;
  var _curWeek = 1;

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  /// 周数改变事件
  void _handleWeekChange(int week) {
    setState(() {
      _week = week;
    });
    print("week change");
  }

  void _handleWeekBack() {
    setState(() {
      _week = _curWeek;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Column(
          children: [
            Text('第\t${_week.toString()}\t周'),
            Text(
              '大二 第二学期 2018级软件工程班',
              style: TextStyle(fontSize: 12.0),
            ),
          ],
        ),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.calendar_today_outlined),
          onPressed: () {
            _scaffoldKey.currentState.openDrawer();

            /// 显示左侧选项栏
          },
        ),
      ),
      body: SlideWidget(
        onWeekChange: _handleWeekChange,
        offset: _week,
      ),
    );
  }
}
