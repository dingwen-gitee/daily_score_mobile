import 'package:daily_score_mobile/dao/course_role_dao.dart';
import 'package:daily_score_mobile/model/course_role_model.dart';
import 'package:daily_score_mobile/widget/chart/pie_chart_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/// 课程规则信息页面
/// @author: dingwen
/// @date: 2021/4/17

class CoursePage extends StatefulWidget {
  @override
  _CoursePageState createState() => _CoursePageState();
}

class _CoursePageState extends State<CoursePage> {
  //课程规则信息List
  List<CourseRoleModel> courseRoleModelList = [];

  @override
  void initState() {
    super.initState();
    fetchData();
  }

  /// 获取服务器数据
  Future<void> fetchData() async {
    Map params = {"pageNum": 1, "pageSize": 100000};
    CourseRoleDao.getCourseRoleList(data: params).then((value) => {
          setState(() {
            courseRoleModelList = value;
          })
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('课程中心'),
      ),
      body: _buildCard(),
    );
  }

  /// 构建通知card
  Widget _buildCard() {
    return ListView.builder(
      itemCount: courseRoleModelList.length,
      itemBuilder: (BuildContext context, int index) {
        return Container(
          margin:
              EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0, bottom: 10.0),
          child: PieChartWidget(
            courseRoleModel: courseRoleModelList[index],
          ),
        );
      },
    );
  }
}
