import 'package:daily_score_mobile/constant/router_constant.dart';
import 'package:daily_score_mobile/page/home/widget/banner_widget.dart';
import 'package:daily_score_mobile/page/home/widget/drawer_widget.dart';
import 'package:daily_score_mobile/page/home/widget/news_card_widget.dart';
import 'package:daily_score_mobile/page/home/widget/option_card_widget.dart';
import 'package:daily_score_mobile/page/home/widget/title_widget.dart';
import 'package:daily_score_mobile/util/date_util.dart';
import 'package:daily_score_mobile/widget/menu/simple_drop_menu_widget.dart';
import 'package:flutter/material.dart';
import 'package:daily_score_mobile/router/router.dart' as MyRouter;

/// 首页
///Created by dingwen on 2021/4/12.
class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Widget> _widgets = [
    BannerWidget(),
    OptionCardWidget(),
    TitleWidget(
      title: '校园资讯',
      subTitle: DateUtil.getCurrentTime(),
      leading: Icon(
        Icons.local_offer,
        color: Colors.red,
        size: 32,
      ),
    ),
    NewsCardWidget()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(236, 240, 241, 1),
      appBar: AppBar(
        // 去掉默认的返回键
        automaticallyImplyLeading: false,
        actions: [SimpleDropMenuWidget()],
        title: Text(
          '首页',
          style: TextStyle(fontFamily: 'Roboto', color: Colors.black),
        ),
        centerTitle: true,
        leading:  Builder(builder: (context){
          return InkWell(
              onTap: (){
                Scaffold.of(context).openDrawer();

              },
              child:Icon(Icons.subject)
          );
        }),
      ),
      drawer: DrawerWidget(),
      body: RefreshIndicator(
        displacement: 40,
        //指示器显示时距顶部位置
        color: Colors.amber,
        //指示器颜色，默认ThemeData.accentColor
        notificationPredicate: defaultScrollNotificationPredicate,
        //是否应处理滚动通知的检查（是否通知下拉刷新动作
        onRefresh: _onRefresh,
        child: ListView.builder(
            itemCount: _widgets.length,
            itemBuilder: (BuildContext context, int index) {
              return _widgets[index];
            }),
      ),
    );
  }

  // _onRefresh 下拉刷新回调
  Future<Null> _onRefresh() {
    return Future.delayed(Duration(seconds: 2), () {
      // 延迟5s完成刷新
      setState(() {});
    });
  }
}
