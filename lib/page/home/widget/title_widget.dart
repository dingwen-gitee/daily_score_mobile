import 'package:flutter/material.dart';

/// 校园资讯标题
///Created by dingwen on 2021/4/13.

class TitleWidget extends StatefulWidget {
  final String title;
  final String subTitle;
  final Widget leading;

  TitleWidget({this.title, this.subTitle, this.leading})
      : assert(title != null && subTitle != null && leading != null);

  @override
  _TitleWidgetState createState() => _TitleWidgetState();
}

class _TitleWidgetState extends State<TitleWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 16.0),
      width: MediaQuery.of(context).size.width,
      height: 40,
      child: ListTile(
          title: Text('${widget.title}',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
          subtitle: Text('${widget.subTitle}'),
          leading: widget.leading),
    );
  }
}
