import 'package:daily_score_mobile/constant/router_constant.dart';
import 'package:daily_score_mobile/router/bundle.dart';
import 'package:flutter/material.dart';
import 'package:daily_score_mobile/router/router.dart' as MyRouter;

/// card
///Created by dingwen on 2021/4/13.
class OptionCardWidget extends StatelessWidget {
  // 功能图表集合
  final Map<String, List> _icons = {
    '学校资讯': [Icons.location_city,'https://www.ynnu.edu.cn/'],
    '课程中心': [Icons.fact_check_sharp,'course_page'],
    '学业成绩': [Icons.auto_awesome,'https://baidu.com/'],
    '我要蹭课': [Icons.six_ft_apart,'https://baidu.com/'],
    '考试安排': [Icons.event,'https://baidu.com/'],
    '空闲教室': [Icons.corporate_fare,'https://baidu.com/'],
    '等级考试': [Icons.golf_course,'https://baidu.com/'],
    '学情警示': [Icons.dangerous,'https://baidu.com/'],
  };
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10.0),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * 0.20,
      child: Card(
        color: Colors.white,
        //z轴的高度，设置card的阴影
        elevation: 8.0,
        //设置shape，这里设置成了R角
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(16.0)),
        ),
        //对Widget截取的行为，比如这里 Clip.antiAlias 指抗锯齿
        clipBehavior: Clip.antiAlias,
        semanticContainer: false,
        child: Wrap(
          spacing: 40, //主轴上子控件的间距
          runSpacing: 6, //交叉轴上子控件之间的间距
          children: _buildOption(context),
        ),
      ),
    );
  }

  List<Widget> _buildOption(BuildContext context) {
    List<Widget> _iconList = [];
    _icons.forEach((title, list) {
      _iconList.add(Tooltip(
        message: '$title',
        child: InkWell(
          onTap: () {
            if (list[1] == "course_page"){
              MyRouter.Router.pageTo(course_page,context: context);
            }else {
              Bundle bundle = Bundle();
              bundle.putString("url",list[1]);
              MyRouter.Router.pageTo(web_site_page,
                  context: context,
                  routeSettings: RouteSettings(arguments: bundle));
            }

          },
          child: Container(
            margin: EdgeInsets.fromLTRB(15.0, 15.0, 0.0, 15.0),
            child: Column(
              children: [
                Icon(list[0], size: 40, color: Theme.of(context).accentColor),
                Text('$title',
                    style: TextStyle(
                      fontFamily: 'Roboto',
                      fontSize: 14.0,
                      color: Colors.black,
                    )),
              ],
            ),
          ),
        ),
      ));

    });
    return _iconList;
  }
}
