import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

/// 首页轮播图
///Created by dingwen on 2021/4/13.

class BannerWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * 0.3,
      child: Swiper(
          itemBuilder: (BuildContext context, int index) {
            return new Image.asset(
              "assets/images/banner0${index+1}.jpg",
              fit: BoxFit.fill,
            );
          },
          itemCount: 4,
          itemWidth: MediaQuery.of(context).size.width,
          pagination: new SwiperPagination(),
          control: new SwiperControl(),
          autoplay: true,
          duration: 300,
          // 用户有操作的时候停止自动播放
          autoplayDisableOnInteraction: true,
        layout: SwiperLayout.STACK,),
    );
  }
}
