import 'package:daily_score_mobile/constant/api_constant.dart';
import 'package:daily_score_mobile/constant/router_constant.dart';
import 'package:daily_score_mobile/dao/student_dao.dart';
import 'package:daily_score_mobile/model/student_info_model.dart';
import 'package:daily_score_mobile/util/preference_utils.dart';
import 'package:daily_score_mobile/widget/loading/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:daily_score_mobile/router/router.dart' as MyRouter;

class DrawerWidget extends StatefulWidget {
  @override
  _DrawerWidgetState createState() => _DrawerWidgetState();
}

class _DrawerWidgetState extends State<DrawerWidget> {
  // 学生信息
  StudentInfoModel _studentInfoModel;
  bool _loading = true;

  @override
  void initState() {
    super.initState();
    fetchData();
  }

  /// 获取服务器数据
  Future<void> fetchData() async {
    PreferenceUtils.instance.getString("studentId").then((studentId) {
      Map params = {"pageNum": 1, "pageSize": 1, "studentId": studentId};
      StudentDao.getStudentInfo(data: params).then((studentInfoModel) => {
            setState(() {
              _studentInfoModel = studentInfoModel;
              _loading = false;
            })
          });
    });
  }

  @override
  Widget build(BuildContext context) {
    return LoadingWidget(
      isLoading: _loading,
      child: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: Text('${_studentInfoModel?.studentName}',
                  style: TextStyle(fontWeight: FontWeight.bold)),
              accountEmail: Text(
                '${_studentInfoModel?.studentQq}@qq.com\n${_studentInfoModel?.clazzYear}级${_studentInfoModel?.clazzName}',
                style: TextStyle(fontSize: 12.0),
              ),
              currentAccountPicture: CircleAvatar(
                backgroundImage:
                    NetworkImage("$base_uri/${_studentInfoModel?.studentImg}"),
              ),
              decoration: BoxDecoration(
                  color: Colors.yellow[400],
                  image: DecorationImage(
                      image: NetworkImage(
                          'https://img2.baidu.com/it/u=4187064469,2018795051&fm=15&fmt=auto&gp=0.jpg'),
                      fit: BoxFit.cover,
                      colorFilter: ColorFilter.mode(
                          Colors.yellow[400].withOpacity(0.65),
                          BlendMode.lighten))),
            ),
            ListTile(
              title: Text(
                '帮助',
                textAlign: TextAlign.left,
              ),
              leading: Icon(
                Icons.help_center,
                color: Colors.green,
                size: 20.0,
              ),
              onTap: () => Navigator.pop(context),
              dense: true,
            ),
            ListTile(
              title: Text(
                '设置',
                textAlign: TextAlign.left,
              ),
              leading: Icon(
                Icons.settings,
                color: Colors.black38,
                size: 20.0,
              ),
              onTap: () => Navigator.pop(context),
            ),
            ListTile(
              title: Text(
                '退出登录',
                textAlign: TextAlign.left,
              ),
              leading: Icon(
                Icons.logout,
                color: Colors.red,
                size: 20.0,
              ),
              onTap: () => MyRouter.Router.pageTo(login_page, context: context),
            ),
          ],
        ),
      ),
    );
  }
}
