import 'package:flutter/material.dart';
/// 校园新闻
///Created by dingwen on 2021/4/13.
class NewsCardWidget extends StatefulWidget {
  @override
  _NewsCardWidgetState createState() => _NewsCardWidgetState();
}

class _NewsCardWidgetState extends State<NewsCardWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10.0),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * 0.21,
      child: Card(
        color: Colors.white,
        //z轴的高度，设置card的阴影
        elevation: 8.0,
        //设置shape，这里设置成了R角
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(16.0)),
        ),
        //对Widget截取的行为，比如这里 Clip.antiAlias 指抗锯齿
        clipBehavior: Clip.antiAlias,
        semanticContainer: false,
        child: Container(
          margin: EdgeInsets.all(20.0),
          child: ListView(
            children: [
              Center(child: Text("云南师范大学2021年公开招聘工作人员公告\n\n",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16.0),)),
              Text("\t\t\t\t根据《云南省事业单位公开招聘工作人员办法》（云人社发〔2016〕182号）等有关文件规定，结合工作需要，经云南省教育厅、云南省人力资源和社会保障厅批准，2021年云南师范大学计划面向社会公开招聘工作人员142名。招聘遵循“公开、平等、竞争、择优”的原则，面向社会，公开报名，统一考试（考核），综合评定，择优聘用。",
              style: TextStyle(fontSize: 14.0),),
            ],
          ),
        ),
      ),
    );
  }
}
