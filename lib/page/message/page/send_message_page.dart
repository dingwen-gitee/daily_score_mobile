import 'package:daily_score_mobile/widget/menu/simple_drop_menu_widget.dart';
import 'package:flutter/material.dart';

/// 消息发送页面
/// @author: dingwen
/// @date: 2021/4/17

class SendMessagePage extends StatefulWidget {
  @override
  _SendMessagePageState createState() => _SendMessagePageState();
}

class _SendMessagePageState extends State<SendMessagePage> {
  TextEditingController _textController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(236, 240, 241, 1),
      appBar: AppBar(
        title: Text('陈兴健'),
        actions: [
          SimpleDropMenuWidget(),
        ],
      ),
      body: Column(
        children: [
          _buildTitleTime(),
          _buildFriendBox(),
          Expanded(
            child:  _buildMyBox(),
          ),
          _buildTextComposer(),
        ],
      ),
    );
  }

  /// 构建时间标题
  Widget _buildTitleTime() {
    return Center(
      child: Container(
          margin: EdgeInsets.only(top: 20.0),
          child: Text(
            '4月17日 下午15:00',
            style: TextStyle(color: Colors.grey),
          )),
    );
  }

  /// 朋友聊天框
  Widget _buildFriendBox() {
    return ListTile(
      leading: ClipOval(
          child: Image.network(
              'https://img1.baidu.com/it/u=3899180969,1118613876&fm=26&fmt=auto&gp=0.jpg')),
      title: Text('陈兴健'),
      subtitle: Container(
        margin: EdgeInsets.only(top: 10.0),
        height: 75,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(
              Radius.circular(10.0),
            ),
            boxShadow: [
              BoxShadow(
                  color: Colors.black12,
                  offset: Offset(0.0, 15.0), //阴影xy轴偏移量
                  blurRadius: 15.0, //阴影模糊程度
                  spreadRadius: 1.0 //阴影扩散程度
                  )
            ]),
        child: Container(
          margin: EdgeInsets.all(10.0),
          child: Text(
            '受到党纪、政纪处分影响期未满或者正在接受纪律审查的人员，以及受到刑事处罚期限未满或者正在接受司法调查尚未做出结论的人员。',
            style: TextStyle(fontSize: 16.0, color: Colors.black),
          ),
        ),
      ),
    );
  }

  // 我的聊天框
  Widget _buildMyBox() {
    return ListTile(
      trailing: ClipOval(
          child: Image.network(
              'https://img2.baidu.com/it/u=3407540227,3011760197&fm=26&fmt=auto&gp=0.jpg')),
      title: Container(
        margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 1.6),
        child: Text('陈永晴'),
      ),
      subtitle: Container(
        margin: EdgeInsets.only(top: 10.0),
        height: 75,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(
              Radius.circular(10.0),
            ),
            boxShadow: [
              BoxShadow(
                  color: Colors.black12,
                  offset: Offset(0.0, 15.0), //阴影xy轴偏移量
                  blurRadius: 15.0, //阴影模糊程度
                  spreadRadius: 1.0 //阴影扩散程度
                  )
            ]),
        child: Container(
          margin: EdgeInsets.all(10.0),
          child: Text(
            '受到党纪、政纪处分影响期未满或者正在接受纪律审查的人员，以及受到刑事处罚期限未满或者正在接受司法调查尚未做出结论的人员。',
            style: TextStyle(fontSize: 16.0, color: Colors.black),
          ),
        ),
      ),
    );
  }

  ///发送信息
  void _handleSubmitted(String text) {
    _textController.clear(); //清空文本框
  }

  //构造输入框
  Widget _buildTextComposer() {
    return   Container(
      margin: EdgeInsets.all(20.0),
        child: Row(children: <Widget>[
          Container(child: Icon(Icons.multitrack_audio_outlined),margin: EdgeInsets.only(right: 20.0),),
      Flexible(
          child: TextField(
        controller: _textController,
        onSubmitted: _handleSubmitted,
        decoration: InputDecoration(
          contentPadding: const EdgeInsets.symmetric(vertical: 10.0),
          hintText: "请输入内容",
          border: InputBorder.none,
        ),
      )),
      Container(
        margin: EdgeInsets.symmetric(horizontal: 4.0),
        child: IconButton(
            icon: Icon(
              Icons.send,
              color: Colors.blue,
            ),
            onPressed: () => _handleSubmitted(_textController.text)),
      )
    ]));
  }
}
