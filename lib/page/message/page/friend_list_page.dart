import 'package:daily_score_mobile/constant/router_constant.dart';
import 'package:daily_score_mobile/model/user_model.dart';
import 'package:flutter/material.dart';
import 'package:daily_score_mobile/router/router.dart' as MyRouter;
/// 好友列表
/// @author: dingwen
/// @date: 2021/4/17

class FriendListPage extends StatefulWidget {
  @override
  _FriendListPageState createState() => _FriendListPageState();
}

class _FriendListPageState extends State<FriendListPage> {
  final List<UserModel> friendList = [
    UserModel(name: "陈兴健",img: 'https://img0.baidu.com/it/u=737398139,3977819174&fm=26&fmt=auto&gp=0.jpg'),
    UserModel(name: "黄奕超",img: 'https://img1.baidu.com/it/u=2386642977,2435093162&fm=26&fmt=auto&gp=0.jpg'),
    UserModel(name: "梁明珠",img: 'https://img1.baidu.com/it/u=1365983964,600966652&fm=26&fmt=auto&gp=0.jpg'),
    UserModel(name: "张向星",img: 'https://img2.baidu.com/it/u=1246108031,1224419100&fm=26&fmt=auto&gp=0.jpg'),
    UserModel(name: "康勇",img: 'https://img1.baidu.com/it/u=3386650602,3522486362&fm=26&fmt=auto&gp=0.jpg'),
    UserModel(name: "吴加娥",img: 'https://img2.baidu.com/it/u=2006465598,4103818545&fm=26&fmt=auto&gp=0.jpg'),
    UserModel(name: "黄云飞",img: 'https://img0.baidu.com/it/u=3778096124,2012774105&fm=26&fmt=auto&gp=0.jpg'),
    UserModel(name: '魏志新',img: 'https://img1.baidu.com/it/u=1640836759,2812172986&fm=26&fmt=auto&gp=0.jpg'),
    UserModel(name: '谭浩成',img: 'https://img2.baidu.com/it/u=3407540227,3011760197&fm=26&fmt=auto&gp=0.jpg'),
    UserModel(name: '巫语萱',img: 'https://img0.baidu.com/it/u=2524943504,1707298546&fm=26&fmt=auto&gp=0.jpg'),
    UserModel(name: '胡太梅',img: 'https://img0.baidu.com/it/u=3127287079,1761374136&fm=26&fmt=auto&gp=0.jpg'),
    UserModel(name: '蔡佩谷',img: 'https://img2.baidu.com/it/u=3556131912,1838882889&fm=26&fmt=auto&gp=0.jpg'),
    UserModel(name: '朱安敏',img: 'https://img1.baidu.com/it/u=2685992266,630870756&fm=26&fmt=auto&gp=0.jpg'),
  ];
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: friendList.length,
        itemBuilder: (context, index) {
          return InkWell(
            onTap: (){
             MyRouter.Router.pageTo(send_message_page,context: context);
            },
            child: Column(
              children: [
                ListTile(
                  leading: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(20.0)),
                    child: Image.network(friendList[index].img),
                  ),
                  title: Text('${friendList[index].name}'),
                  trailing: Icon(Icons.keyboard_arrow_right),
                ),
                Divider()
              ],
            ),
          );
        });
  }
}
