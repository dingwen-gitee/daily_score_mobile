import 'package:daily_score_mobile/model/user_model.dart';
import 'package:flutter/material.dart';
/// TODO
/// @author: dingwen
/// @date: 2021/4/17

class RecommendFriendPage extends StatefulWidget {
  @override
  _RecommendFriendPageState createState() => _RecommendFriendPageState();
}

class _RecommendFriendPageState extends State<RecommendFriendPage> {
  final List<UserModel> friendList = [
    UserModel(name: "陈兴健",img: 'https://img0.baidu.com/it/u=737398139,3977819174&fm=26&fmt=auto&gp=0.jpg'),
    UserModel(name: "黄奕超",img: 'https://img1.baidu.com/it/u=2386642977,2435093162&fm=26&fmt=auto&gp=0.jpg'),
  ];
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: friendList.length,
        itemBuilder: (context, index) {
          return InkWell(
            onTap: (){

            },
            child: Column(
              children: [
                ListTile(
                  leading: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(20.0)),
                    child: Image.network(friendList[index].img),
                  ),
                  title: Text('${friendList[index].name}'),
                  trailing: Icon(Icons.add_box_outlined,color: Colors.green,),
                ),
                Divider()
              ],
            ),
          );
        });
  }
}
