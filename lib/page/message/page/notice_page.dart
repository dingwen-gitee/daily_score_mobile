import 'package:daily_score_mobile/widget/menu/simple_drop_menu_widget.dart';
import 'package:flutter/material.dart';

/// 通知消息
/// @author: dingwen
/// @date: 2021/4/17

class NoticePage extends StatefulWidget {
  @override
  _NoticePageState createState() => _NoticePageState();
}

class _NoticePageState extends State<NoticePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(236, 240, 241, 1),
      appBar: AppBar(
        title: Text('通知'),
        actions: [
          SimpleDropMenuWidget(),
        ],
      ),
      body: Column(
        children: [
          _buildTitleTime(),
          _buildNotice(),
        ],
      ),
    );
  }

  /// 构建通知card
  Widget _buildNotice() {
    return Card(
      color: Colors.white,
      shadowColor: Colors.grey,
      elevation: 20.0,
      borderOnForeground: false,
      margin: EdgeInsets.all(10.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(12.0)),),
      child: Container(
        width: MediaQuery.of(context).size.width / 1.08,
        height: MediaQuery.of(context).size.height / 3.6,
        alignment: Alignment.center,
        child: Column(
          children: [
            Container(
                margin: EdgeInsets.all(10.0), child: Text('关于用户登录修改密码的通知',style: TextStyle(fontSize: 16.0,fontWeight: FontWeight.bold),)),
            Container(margin: EdgeInsets.all(10.0), child: Text('\t\t大学生计算机课程日常成绩管理通知，为安全考虑。初始用户密码为学号，请登录后修改密码。')),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height / 6,
              child: Image.network(
                  'https://img1.baidu.com/it/u=3877577004,1599025994&fm=26&fmt=auto&gp=0.jpg'),
            )
          ],
        ),
      ),
    );
  }

  /// 构建时间标题
  Widget _buildTitleTime() {
    return Center(
      child: Container(
          margin: EdgeInsets.only(top: 20.0),
          child: Text(
            '4月17日 下午15:00',
            style: TextStyle(color: Colors.grey),
          )),
    );
  }
}
