import 'package:daily_score_mobile/page/message/page/recommend_friend_page.dart';
import 'package:flutter/material.dart';

import 'friend_list_page.dart';
/// TODO
/// @author: dingwen
/// @date: 2021/4/16
class ChatPage extends StatefulWidget {
  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> with SingleTickerProviderStateMixin {
  TabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = new TabController(
        vsync: this,//固定写法
        length: 2   //指定tab长度
    );

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('聊天'),
        actions: [
          Container(margin: EdgeInsets.all(10.0),child: Icon(Icons.search_sharp)),
          Container(margin: EdgeInsets.all(10.0),child: Icon(Icons.add_circle_outline_outlined)),
        ],
        bottom: _getBottom(),
      ),
      body: _getPages(),
    );
  }

  /// 构建tabBar
  PreferredSizeWidget _getBottom() {
      return TabBar(
        controller: _tabController,
        isScrollable: false,
        unselectedLabelColor: Colors.black38,
        indicatorColor: Colors.black54,
        indicatorSize: TabBarIndicatorSize.label,
        indicatorWeight: 1.0,
        tabs: < Widget > [
          Tab(icon: Icon(Icons.list),),
          Tab(icon: Icon(Icons.lightbulb),),
        ],
      );
  }

  /// 构建页面
  Widget _getPages() {
    return TabBarView(
      controller: _tabController,
      children: < Widget > [
        FriendListPage(),
        RecommendFriendPage()
      ],
    );
  }
}
