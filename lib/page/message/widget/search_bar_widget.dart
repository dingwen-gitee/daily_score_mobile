import 'package:flutter/material.dart';
import 'package:flutter_search_bars/flutter_search_bars.dart';
/// 消息页 搜索
/// @author: dingwen
/// @date: 2021/4/15

class SearchBarWidget extends StatefulWidget {
  @override
  _SearchBarWidgetState createState() => _SearchBarWidgetState();
}

class _SearchBarWidgetState extends State<SearchBarWidget> {
  @override
  Widget build(BuildContext context) {
    return  Stack(
      children: [
        Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height * 0.07,
          color: Colors.yellow,
        ),
        Container(
          margin: EdgeInsets.only(
              left: MediaQuery.of(context).size.width / 2 - 200),
          width: 400.0,
          height: 60.0,
          child: SearchStaticBar(
            heroTag: "searchBar",
            clickCallBack: () {},
          ),
        )
      ],
    );
  }
}
