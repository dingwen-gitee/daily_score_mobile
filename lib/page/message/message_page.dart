import 'package:daily_score_mobile/constant/router_constant.dart';
import 'package:daily_score_mobile/page/message/widget/search_bar_widget.dart';
import 'package:daily_score_mobile/widget/menu/popup_menu_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:daily_score_mobile/router/router.dart' as MyRouter;

/// 消息页面
///Created by dingwen on 2021/4/12.
class MessagePage extends StatefulWidget {
  @override
  _MessagePageState createState() => _MessagePageState();
}

class _MessagePageState extends State<MessagePage> {
  /// 小菜单集合
  Map<String, Widget> popupMenuMap = {};

  @override
  void initState() {
    super.initState();
    popupMenuMap.putIfAbsent(
        "发起群聊",
        () => Row(
              children: [
                Icon(
                  Icons.person_add_alt_1_rounded,
                  color: Colors.white,
                ),
                Text(
                  "发起群聊",
                  style: TextStyle(color: Colors.white),
                ),
              ],
            ));
    popupMenuMap.putIfAbsent(
        "添加朋友",
        () => Row(
              children: [
                Icon(
                  Icons.perm_identity,
                  color: Colors.white,
                ),
                Text(
                  "添加朋友",
                  style: TextStyle(color: Colors.white),
                ),
              ],
            ));
    popupMenuMap.putIfAbsent(
        "扫一扫",
        () => Row(
              children: [
                Icon(
                  Icons.camera_alt_outlined,
                  color: Colors.white,
                ),
                Text(
                  "\t扫一扫",
                  style: TextStyle(color: Colors.white),
                ),
              ],
            ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(236, 240, 241, 1),
      appBar: AppBar(
        elevation: 0.0,
        automaticallyImplyLeading: false,
        title: Text("消息(2)"),
        actions: [
          Container(
            margin: EdgeInsets.only(right: 10.0),
            child: Icon(
              Icons.person_add,
              size: 26.0,
            ),
          ),
          PopupMenuWidget(
            popupMenuMap: popupMenuMap,
          ),
        ],
      ),
      body: Container(
        margin: EdgeInsets.only(bottom: 26.0),
        child: ListView(
          children: _buildMessage(),
        ),
      ),
    );
  }

  /// 构建消息主体
  List<Widget> _buildMessage() {
    List<Widget> widgets = [];
    widgets.add(SearchBarWidget());
    widgets.addAll(_buildMessageItemList());
    return widgets;
  }

  /// 构建消息ItemList
  List<Widget> _buildMessageItemList() {
    List<Widget> widgets = [];
    double iconSize = 30.0;
    List<Color> colorList = [
      Colors.yellow,
      Colors.green,
      Colors.red,
      Colors.yellow,
      Colors.blue,
      Colors.blue,
      Colors.yellow,
      Colors.yellow,
      Colors.green,
      Colors.yellow,
    ];
    List<Icon> iconList = [
      Icon(Icons.notification_important, color: Colors.black, size: iconSize),
      Icon(
        Icons.help,
        color: Colors.white,
        size: iconSize,
      ),
      Icon(
        Icons.message_rounded,
        color: Colors.white,
        size: iconSize,
      ),
      Icon(Icons.ac_unit, color: Colors.black, size: iconSize),
      Icon(Icons.baby_changing_station, color: Colors.white, size: iconSize),
      Icon(Icons.cake, color: Colors.white, size: iconSize),
      Icon(Icons.data_usage_sharp, color: Colors.black, size: iconSize),
      Icon(Icons.eco, color: Colors.black, size: iconSize),
      Icon(Icons.face_rounded, color: Colors.white, size: iconSize),
      Icon(Icons.keyboard, color: Colors.black, size: iconSize),
    ];
    List<String> titleList = [
      '通知消息',
      '用户私信',
      '校园情报站',
      '测试模块一',
      '测试模块二',
      '测试模块三',
      '测试模块四',
      '测试模块五',
      '测试模块六',
      '测试模块七'
    ];
    List<String> subtitle = [
      '系统服务',
      '系统服务',
      '系统服务',
      '系统服务',
      '系统服务',
      '系统服务',
      '系统服务',
      '系统服务',
      '系统服务',
      '系统服务'
    ];
    List<Widget> trailingList = [
      Text('今天'),
      Text('昨天'),
      Text('今天'),
      Text('今天'),
      Text('今天'),
      Text('今天'),
      Text('今天'),
      Text('今天'),
      Text(''),
      Text('今天')
    ];
    for (int index = 0; index < colorList.length; index++) {
      widgets.add(_buildMessageItem(
          bgColor: colorList[index],
          title: titleList[index],
          subtitle: subtitle[index],
          icon: iconList[index],
          trailing: trailingList[index],index: index),);
    }
    return widgets;
  }

  /// 构建消息Item
  Widget _buildMessageItem(
      {Color bgColor,
      Icon icon,
      String title,
      String subtitle,
      Widget trailing,
      int index}) {
    return InkWell(
      onTap: () {
        switch(index){
          case 0 :
            // 通知
            MyRouter.Router.pageTo(notice_page,context: context);
            break;
          case 1 :
            // 私信
            MyRouter.Router.pageTo(chat_page,context: context);
            break;
        }
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: 75.0,
        decoration: BoxDecoration(
            color: Colors.white,
            border:
                Border(bottom: BorderSide(width: 1, color: Color(0xffe5e5e5)))),
        child: ListTile(
          leading: Container(
            width: 50.0,
            height: 50.0,
            decoration: BoxDecoration(
              color: bgColor,
              borderRadius: BorderRadius.all(Radius.circular(50.0)),
            ),
            child: icon,
          ),
          title: Text(
            '$title',
          ),
          subtitle: Text('$subtitle'),
          trailing: trailing,
        ),
      ),
    );
  }
}
