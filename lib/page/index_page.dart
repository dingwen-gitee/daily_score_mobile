import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:daily_score_mobile/router/router.dart' as MyRouter;
import 'package:daily_score_mobile/widget/protocol/protocol_widget.dart';
import 'package:daily_score_mobile/constant/router_constant.dart';

/// 首页
///Created by dingwen on 2021/4/12.

class IndexPage extends StatefulWidget {
  @override
  _IndexPageState createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage> {
  @override
  void initState() {
    super.initState();
    // 加载用户协议
    Future.delayed(Duration(milliseconds: 800), () => userProtocolGo());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Image.asset('assets/images/course.png'),
      ),
    );
  }

  ///当用户没有同意用户协议时，关闭当前App
  void closeApp() =>
      SystemChannels.platform.invokeMethod("SystemNavigator.pop");

  void userProtocolGo() async {
    if (await ProtocolWidget().showUserProtocol(context)) {
      //进入
      next();
    } else {
      closeApp();
    }
  }

  void next() async {
    MyRouter.Router.pageTo(splash_page, context: context);
  }
}
