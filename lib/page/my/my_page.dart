import 'package:daily_score_mobile/page/my/widget/body_menu_widget.dart';
import 'package:daily_score_mobile/page/my/widget/head_stack_widget.dart';
import 'package:flutter/material.dart';

/// 我的页面
///Created by dingwen on 2021/4/12.

class MyPage extends StatefulWidget {
  @override
  _MyPageState createState() => _MyPageState();
}

class _MyPageState extends State<MyPage> {
  final List<Widget> _widgets = [HeadStackWidget(),BodyMenuWidget()];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text('个人中心'),
            centerTitle: true,
            elevation: 0.0,
            automaticallyImplyLeading: false),
        backgroundColor: Color.fromRGBO(236, 240, 241, 1),
        body: RefreshIndicator(
          onRefresh: _onRefresh,
          //指示器显示时距顶部位置
          color: Colors.amber,
          //指示器颜色，默认ThemeData.accentColor
          notificationPredicate: defaultScrollNotificationPredicate,
          //是否应处理滚动通知的检查（是否通知下拉刷新动作
          child: ListView.builder(
              itemCount: 2,
              itemBuilder: (BuildContext context, int index) {
                return _widgets[index];
              }),
        ));
  }

  // _onRefresh 下拉刷新回调
  Future<Null> _onRefresh() {
    return Future.delayed(Duration(seconds: 2), () {
      // 延迟5s完成刷新
      setState(() {});
    });
  }
}
