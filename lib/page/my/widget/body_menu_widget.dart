import 'package:daily_score_mobile/constant/router_constant.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:daily_score_mobile/router/router.dart' as MyRouter;

/// 个人中心菜单
/// @author: dingwen
/// @date: 2021/4/14

class BodyMenuWidget extends StatefulWidget {
  @override
  _BodyMenuWidgetState createState() => _BodyMenuWidgetState();
}

class _BodyMenuWidgetState extends State<BodyMenuWidget> {
  List<String> _titleList = ['客服中心', '账号安全', '设置', '帮助与反馈', '关于我们', '修改密码','退出登录'];
  List<Icon> _iconList = [
    Icon(Icons.headset_mic, color: Colors.blue),
    Icon(
      Icons.security,
      color: Colors.red,
    ),
    Icon(
      Icons.app_settings_alt,
      color: Colors.black,
    ),
    Icon(
      Icons.help,
      color: Colors.green,
    ),
    Icon(
      Icons.info,
      color: Colors.blue,
    ),
    Icon(
      Icons.lock_open,
      color: Colors.red,
    ),
    Icon(
      Icons.logout,
      color: Colors.redAccent,
    )
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 20.0),
      color: Colors.white,
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * 0.6,
      child: ListView.builder(
          itemCount: _titleList.length,
          itemBuilder: (BuildContext context, int index) {
            return Material(
              child: Ink(
                child: InkWell(
                  onTap: () {
                    switch (index) {
                      case 0:
                        _functionPersonalInformation();
                        break;
                      case 1:
                        _functionSecurity();
                        break;
                      case 2:
                        _functionSetting();
                        break;
                      case 3 :
                        _functionHelp();
                        break;
                      case 4:
                        _functionAbout();
                        break;
                      case 5:
                        _functionPass();
                        break;
                      case 6:
                        _functionLogout();
                        break;
                    }
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                width: 1, color: Color(0xffe5e5e5)))),
                    child: ListTile(
                      leading: _iconList[index],
                      title: Text("${_titleList[index]}"),
                      trailing: Icon(Icons.keyboard_arrow_right),
                    ),
                  ),
                ),
              ),
            );
          }),
    );
  }

  /// 个人信息
  void _functionPersonalInformation() {}

  /// 账号安全
  void _functionSecurity() {}

  /// 设置
  void _functionSetting() {}

  /// 帮助 & 反馈
  void _functionHelp() {}

  /// 关于我们
  void _functionAbout() {}

  /// 修改面膜
  void _functionPass() {}

  /// 退出登录
  void _functionLogout() {
    showCupertinoAlertDialog();
  }

  /// 退出登录确认框
  void showCupertinoAlertDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CupertinoAlertDialog(
            title: Text("您确定退出登录吗？"),
            content: Column(
              children: <Widget>[
                SizedBox(
                  height: 10,
                ),
                Align(
                  child: Text("退出登录将收不到系统消息，下次你需要输入账号密码重新登录！"),
                  alignment: Alignment(0, 0),
                ),
              ],
            ),
            actions: <Widget>[
              CupertinoDialogAction(
                child: Text("取消"),
                onPressed: () {
                  Navigator.pop(context);
                  print("取消");
                },
              ),
              CupertinoDialogAction(
                child: Text("确定"),
                onPressed: () {
                  //TODO 执行退出登录逻辑
                  MyRouter.Router.pageTo(login_page,context: context);
                },
              ),
            ],
          );
        });
  }
}
