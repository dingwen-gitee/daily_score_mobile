import 'dart:convert';

import 'package:daily_score_mobile/constant/api_constant.dart';
import 'package:daily_score_mobile/dao/student_dao.dart';
import 'package:daily_score_mobile/model/student_info_model.dart';
import 'package:daily_score_mobile/model/student_model.dart';
import 'package:daily_score_mobile/util/preference_utils.dart';
import 'package:daily_score_mobile/widget/loading/loading_widget.dart';
import 'package:flutter/material.dart';

/// 个人中心头部
/// @author: dingwen
/// @date: 2021/4/14

class HeadStackWidget extends StatefulWidget {
  @override
  _HeadStackWidgetState createState() => _HeadStackWidgetState();
}

class _HeadStackWidgetState extends State<HeadStackWidget> {
  // 学生信息
  StudentInfoModel _studentInfoModel;

  // 网络请求是否执行完毕
  bool _loading = true;

  /// 初始化
  @override
  void initState() {
    super.initState();
    fetchData();
  }

  /// 获取服务器数据
  Future<void> fetchData() async {
    PreferenceUtils.instance.getString("studentId").then((studentId) {
      Map params = {"pageNum": 1, "pageSize": 1, "studentId": studentId};
      StudentDao.getStudentInfo(data: params).then((studentInfoModel) => {
            setState(() {
              _studentInfoModel = studentInfoModel;
              _loading = false;
            })
          });
    });
  }

  @override
  Widget build(BuildContext context) {
    return LoadingWidget(
      isLoading: _loading,
      child: Stack(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height * 0.2,
            color: Colors.yellow,
          ),
          Positioned(
            top: MediaQuery.of(context).size.height * 0.08,
            left: MediaQuery.of(context).size.width / 2 - 400,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(400.0)),
                color: Colors.white,
              ),
              width: 800,
              height: 800,
            ),
          ),
          Positioned(
            top: MediaQuery.of(context).size.height * 0.1 - 70,
            left: MediaQuery.of(context).size.width / 2 - 50,
            child: Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.yellow, width: 2.0),
                  borderRadius: BorderRadius.all(Radius.circular(50.0)),
                  color: Colors.white,
                  image: DecorationImage(
                      image: NetworkImage(
                          "$base_uri/${_studentInfoModel?.studentImg}"),
                      fit: BoxFit.cover)),
              width: 100,
              height: 100,
            ),
          ),
          Positioned(
            top: MediaQuery.of(context).size.height * 0.1 + 3 - 70,
            left: MediaQuery.of(context).size.width / 2 - 55,
            child: Container(
              decoration: BoxDecoration(
                border: Border.all(color: Colors.white, width: 4.0),
                borderRadius: BorderRadius.all(Radius.circular(55.0)),
                color: Colors.transparent,
              ),
              width: 110,
              height: 110,
            ),
          ),
          Positioned(
              top: MediaQuery.of(context).size.height * 0.15,
              left: MediaQuery.of(context).size.width / 2 - 27,
              child: Center(
                child: Text(
                  "${_studentInfoModel?.studentName}",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
              ))
        ],
      ),
    );
  }
}
