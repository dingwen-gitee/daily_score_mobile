import 'package:flutter/material.dart';

/// 404页面
///Created by dingwen on 2021/4/12.

class NotFindPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //当前主题的强调颜色
      backgroundColor: Theme.of(context).accentColor,
      body: Container(
        child: Center(
          child: Text('404'),
        ),
      ),
    );
  }
}
