import 'package:flutter/material.dart';

/// 没有数据页面
/// @author: dingwen
/// @date: 2021/5/2
class NotDataPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.white,
        width: MediaQuery
            .of(context)
            .size
            .width,
        height: double.infinity,
        child: InkWell(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height / 2,
                child: Image.asset('assets/images/no_data.png'),
              ),
            ],
          ),
        )
    );
  }
}
