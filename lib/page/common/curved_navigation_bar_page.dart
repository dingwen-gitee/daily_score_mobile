import 'package:daily_score_mobile/page/home/home_page.dart';
import 'package:daily_score_mobile/page/message/message_page.dart';
import 'package:daily_score_mobile/page/my/my_page.dart';
import 'package:daily_score_mobile/page/schedule/schedule_page.dart';
import 'package:daily_score_mobile/page/score/score_page.dart';
import 'package:daily_score_mobile/widget/navigation-bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';

/// 底部导航页面
/// @author: dingwen
/// @date: 2021/1/9

class CuredNavigationBarPage extends StatefulWidget {
  @override
  _CuredNavigationBarPageState createState() => _CuredNavigationBarPageState();
}

class _CuredNavigationBarPageState extends State<CuredNavigationBarPage> {
  ///需要跳转的页面
  List<Widget> pages = [];

  ///底部导航图标和图标对应标题
  Map<String, IconData> icons = {};

  @override
  void initState() {
    super.initState();
    pages.addAll([
      SchedulePage(),
      ScorePage(),
      HomePage(),
      MessagePage(),
      MyPage(),
    ]);
    icons.putIfAbsent('课程表', () => Icons.calendar_today_outlined);
    icons.putIfAbsent('成绩', () => Icons.money);
    icons.putIfAbsent('首页', () => Icons.home);
    icons.putIfAbsent('消息', () => Icons.chat);
    icons.putIfAbsent('我的', () => Icons.person);
  }

  @override
  Widget build(BuildContext context) {
    return CurvedNavigationBarWidget(
      key: ValueKey('app_bar'),
      icons: icons,
      activeColor: Colors.amber,
      pages: pages,
    );
  }
}
