import 'package:daily_score_mobile/router/router.dart' as MyRouter;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:daily_score_mobile/constant/router_constant.dart';
import 'package:liquid_swipe/liquid_swipe.dart';

/// splash
///Created by dingwen on 2021/4/12.

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  ///当前图片显示的下标
  int _currentIndex = 0;

  ///背景图片
  List<String> _imageList = [
    'assets/images/sp01.png',
    'assets/images/sp02.png',
    'assets/images/sp03.png',
    'assets/images/sp04.png'
  ];

  ///当前屏幕宽高
  double width;
  double height;

  ///水滴动画切换
  LiquidController _liquidController = LiquidController();

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;
    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        body: Scaffold(
          backgroundColor: Colors.white,
          body: Stack(
            children: [
              //第一层的背景图片
              _buildFirst(width, height),
              //第二层的点指示
              _buildSecond(),
            ],
          ),
        ));
  }

  ///构建第一层的背景图片
  Positioned _buildFirst(width, height) {
    //Liquid实现
    return Positioned.fill(
        child: LiquidSwipe(
      pages: _buildFirstChild(width, height),
      //动画样式
      waveType: WaveType.liquidReveal,
      //动画控制器
      liquidController: _liquidController,
      //执行动画时忽略用户手势
      ignoreUserGestureWhileAnimating: true,
      //关闭循环播放
      enableLoop: false,
      onPageChangeCallback: (index) => setState(() {
        _currentIndex = index;
        if (index == (_imageList.length - 1)) {
          // 去登录页
          MyRouter.Router.pageTo(login_page,context: context);
        }
      }),
    ));
  }

  ///构建第一层的背景图片 child
  _buildFirstChild(width, height) {
    List<Widget> widgets = [];
    _imageList.forEach((imgStr) {
      widgets.add(Image.asset(
        '$imgStr',
        width: width,
        height: height,
        fit: BoxFit.fill,
      ));
    });
    return widgets;
  }

  ///构建第二层的指示器
  _buildSecond() {
    return Positioned(
        left: 0,
        right: 0,
        bottom: 2,
        height: 44,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: _buildSecondChild(),
        ));
  }

  ///构建第二层的指示器 child
  List<Widget> _buildSecondChild() {
    List<Widget> widgets = [];
    for (int index = 0; index < _imageList.length; index++) {
      widgets.add(AnimatedContainer(
        height: 6,
        width: index == _currentIndex ? 40 : 6,
        margin: EdgeInsets.only(left: 16),
        duration: Duration(milliseconds: 600),
        decoration: BoxDecoration(
            color: Colors.amber,
            borderRadius: BorderRadius.all(Radius.circular(10))),
      ));
    }
    return widgets;
  }
}
