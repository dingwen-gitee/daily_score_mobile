import 'package:daily_score_mobile/page/login/flare/sign_in_button.dart';
import 'package:daily_score_mobile/page/login/flare/tracking_text_input.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';

import 'flare_sign_in_controller.dart';

class FlareSignIn extends StatefulWidget {
  @override
  _FlareSignInState createState() => _FlareSignInState();
}

class _FlareSignInState extends State<FlareSignIn> {
  /// flare 动画控制器
  FlareSignInController _signInController;

  /// 初始化
  @override
  void initState() {
    _signInController = FlareSignInController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: <Widget>[
          Positioned.fill(
            child: Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topRight,
                      end: Alignment.bottomLeft,
                      stops: [
                    0.0,
                    1.0
                  ],
                      colors: [
                    Color.fromRGBO(255, 193, 7, .6),
                    Color.fromRGBO(255, 235, 59, .6),
                  ])),
            ),
          ),
          Positioned.fill(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  height: 260,
                  padding: EdgeInsets.only(left: 16.0, right: 16.0),
                  child: FlareActor(
                    'assets/Teddy.flr',
                    shouldClip: false,
                    alignment: Alignment.bottomCenter,
                    fit: BoxFit.contain,
                    controller: _signInController,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                    left: 16.0,
                    right: 16.0,
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(25.0)),
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(30.0),
                      child: Form(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            TrackingTextInput(
                              label: '账号',
                              hint: '请输入账号',
                              onCaretMoved: (Offset caret) {
                                // 账号输入框获得聚焦时间时眨眼
                                _signInController.lookAt(caret);
                              },
                              onTextChanged: (String value) {
                                // 输入框文本发生改变的时候赋值
                                _signInController.setAccount(value);
                              },
                            ),
                            TrackingTextInput(
                              label: '密码',
                              hint: '请输入密码',
                              isObscured: true,
                              onCaretMoved: (Offset caret) {
                                // 聚焦密码输入框的时候移除眼睛动画
                                _signInController.coverEyes(caret != null);
                                _signInController.lookAt(null);
                              },
                              onTextChanged: (String value) {
                                // 输入框文本发生改变的时候赋值
                                _signInController.setPassword(value);
                              },
                            ),
                            SignInButton(
                              child: Text(
                                '登录',
                                style: TextStyle(
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.bold),
                              ),
                              onPressed: () => _signInController.submit(),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
