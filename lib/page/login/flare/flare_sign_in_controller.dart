import 'dart:math';
import 'dart:ui';

import 'package:daily_score_mobile/constant/router_constant.dart';
import 'package:daily_score_mobile/dao/student_dao.dart';
import 'package:daily_score_mobile/model/student_model.dart';
import 'package:daily_score_mobile/util/preference_utils.dart';
import 'package:flare_flutter/flare.dart';
import 'package:flare_flutter/flare_controls.dart';
import 'package:daily_score_mobile/router/router.dart' as MyRouter;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

/// flare 登录 控制器
///Created by dingwen on 2021/4/12.

class FlareSignInController extends FlareControls {
  ActorNode _faceControl;
  Mat2D _globalToFlareWorld = Mat2D();
  Vec2D _caretGlobal = Vec2D();
  Vec2D _caretWorld = Vec2D();
  Vec2D _faceOrigin = Vec2D();
  Vec2D _faceOriginLocal = Vec2D();
  bool _hasFocus = false;

  /// 登入账号 【学号】
  String _account;

  /// 登入密码
  String _password;

  static const double _projectGaze = 60.0;

  /// 动画监管
  @override
  bool advance(FlutterActorArtboard artboard, double elapsed) {
    super.advance(artboard, elapsed);
    Vec2D targetTranslation;
    if (_hasFocus) {
      Vec2D.transformMat2(_caretWorld, _caretGlobal, _globalToFlareWorld);
      _caretWorld[1] +=
          sin(new DateTime.now().millisecondsSinceEpoch / 300.0) * 70.0;

      Vec2D toCaret = Vec2D.subtract(Vec2D(), _caretWorld, _faceOrigin);
      Vec2D.normalize(toCaret, toCaret);
      Vec2D.scale(toCaret, toCaret, _projectGaze);

      Mat2D toFaceTransform = Mat2D();
      if (Mat2D.invert(toFaceTransform, _faceControl.parent.worldTransform)) {
        Vec2D.transformMat2(toCaret, toCaret, toFaceTransform);
        targetTranslation = Vec2D.add(Vec2D(), toCaret, _faceOriginLocal);
      }
    } else {
      targetTranslation = Vec2D.clone(_faceOriginLocal);
    }

    Vec2D diff =
        Vec2D.subtract(Vec2D(), targetTranslation, _faceControl.translation);
    Vec2D frameTranslation = Vec2D.add(Vec2D(), _faceControl.translation,
        Vec2D.scale(diff, diff, min(1.0, elapsed * 5.0)));

    _faceControl.translation = frameTranslation;

    return true;
  }

  /// 动画管理初始化
  @override
  void initialize(FlutterActorArtboard artboard) {
    super.initialize(artboard);
    _faceControl = artboard.getNode("ctrl_face");
    if (_faceControl != null) {
      _faceControl.getWorldTranslation(_faceOrigin);
      Vec2D.copy(_faceOriginLocal, _faceControl.translation);
    }
    play("idle");
  }

  /// 完成时触发
  @override
  void onCompleted(String name) {
    play("idle");
  }

  @override
  void setViewTransform(Mat2D viewTransform) {
    Mat2D.invert(_globalToFlareWorld, viewTransform);
  }

  /// 眨眼
  void lookAt(Offset caret) {
    if (caret == null) {
      _hasFocus = false;
      return;
    }
    _caretGlobal[0] = caret.dx;
    _caretGlobal[1] = caret.dy;
    _hasFocus = true;
  }

  /// 提交密码
  void setPassword(String value) {
    _password = value;
  }

  /// 提交账号
  void setAccount(String value) {
    _account = value;
  }

  /// 闭眼处理
  bool _isCoveringEyes = false;

  coverEyes(cover) {
    if (_isCoveringEyes == cover) {
      return;
    }
    _isCoveringEyes = cover;
    if (cover) {
      //抬手
      play("hands_up");
    } else {
      // 放手
      play("hands_down");
    }
  }

  /// 提交登录
  void submit() async {
    login().then((value) => {
          if (value)
            {
              play("success"),
              MyRouter.Router.pageTo(curved_navigation_bar_page)
            }
          else
            {play("fail")}
        });
  }

  /// 登录
  Future<bool> login() async {
    if (_account == null) {
      Fluttertoast.showToast(
        msg: "账号不能为空",
      );
      return Future.value(false);
    }
    if (_password == null) {
      Fluttertoast.showToast(
        msg: "密码不能为空",
      );
      return Future.value(false);
    }
    Map student = {"studentId": _account, "studentPassword": _password};
    StudentModel studentModel = await StudentDao.login(data: student);
    if (studentModel != null) {
      PreferenceUtils.instance.saveString("studentId", studentModel.studentId);
      return Future.value(true);
    } else {
      return Future.value(false);
    }
  }
}
