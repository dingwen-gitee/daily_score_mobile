import 'package:daily_score_mobile/constant/router_constant.dart';
import 'package:flutter/material.dart';
import 'package:daily_score_mobile/router/router.dart' as MyRouter;

import 'flare/flare_sign_in.dart';
/// 登录页
///Created by dingwen on 2021/4/12.

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Theme(
        data: Theme.of(context).copyWith(
          primaryColor: Colors.black54,
        ),
        child: FlareSignIn(),
      ),
    );
  }
}
