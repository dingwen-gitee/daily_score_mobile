import 'package:daily_score_mobile/constant/router_constant.dart';
import 'package:daily_score_mobile/widget/menu/simple_drop_menu_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:daily_score_mobile/router/router.dart' as MyRouter;

/// todo
///Created by dingwen on 2021/4/12.

class ScorePage extends StatefulWidget {
  @override
  _ScorePageState createState() => _ScorePageState();
}

class _ScorePageState extends State<ScorePage> {
  List<String> _titleList = ['课程成绩查询', '英语四六级', '普通话水平测试', '全国计算机等级考试'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.yellow[50],
      appBar: AppBar(
        title: Text('成绩'),
        actions: [SimpleDropMenuWidget()],
        automaticallyImplyLeading: false,
        centerTitle: true,
        leading: Icon(Icons.money),
      ),
      body: ListView(
        children: [
          Container(
            margin: EdgeInsets.all(20.0),
            child: Text(
              '请选择你要查询的成绩',
              style: TextStyle(fontSize: 16.0),
            ),
          ),
          Container(
            margin: EdgeInsets.all(20.0),
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height / 4,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(12.0)),
              color: Colors.white,
            ),
            child: Stack(
              children: [
                Column(
                  children: _buildQueryItem(),
                ),
                Positioned(
                  top: 16.0,
                  right: 50.0,
                  child: Icon(
                  Icons.hot_tub,color: Colors.red,),
                )
                ],
            ),
          ),
          Container(
            margin: EdgeInsets.all(20.0),
            child: Text(
              '声明',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
            ),
          ),
          Container(
            margin: EdgeInsets.all(20.0),
            child: Text('\t\t当您使用日常成绩查询成绩功能时，需要您填写相关系统的账号和密码，并授权给平台临时使用，'
                '以方便您查询教务系统成绩、英语四六级成绩、普通话水平测试成绩、全国计算机等级考试成绩。\n\t\t日常成绩不会保存您的账号、密码以及相关成绩信息,并保证不对任何机构或第三方开放。'
                '如果您不同意此行为,您需要点击左上角返回按钮退出。当您进行查询操作时,即代表您同意超级课程表执行此行为。'),
          ),
          Container(
            alignment: Alignment.bottomRight,
            margin: EdgeInsets.only(right: 20.0),
            child: Text('特此声明'),
          ),
        ],
      ),
    );
  }

  /// 构建成绩查询菜单项
  List<Widget> _buildQueryItem() {
    List<Widget> widgets = [];
    for (int index = 0; index < _titleList.length; index++) {
      widgets.add(
        InkWell(
          child: Container(
            decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(width: 1, color: Color(0xffe5e5e5)))),
            child: ListTile(
              title: Text('${_titleList[index]}'),
              trailing: Icon(Icons.keyboard_arrow_right),
              onTap: () {
                switch (index) {
                  case 0:
                    scoreQuerySchool();
                    break;
                  case 1:
                    scoreQueryEnglish();
                    break;
                  case 2:
                    scoreQueryTalk();
                    break;
                  case 3:
                    scoreQueryComputer();
                    break;
                }
              },
            ),
          ),
        ),
      );
    }
    return widgets;
  }

  ///教务系统成绩查询
  scoreQuerySchool() => MyRouter.Router.pageTo(query_page);

  ///英语四六级成绩查询
  scoreQueryEnglish() {}

  ///普通话考试成绩查询
  scoreQueryTalk() {}

  /// 全国计算机等级考试成绩查询
  scoreQueryComputer() {}
}
