import 'package:daily_score_mobile/constant/router_constant.dart';
import 'package:daily_score_mobile/page/login/flare/sign_in_button.dart';
import 'package:daily_score_mobile/router/bundle.dart';
import 'package:daily_score_mobile/widget/menu/simple_drop_menu_widget.dart';
import 'package:flutter/material.dart';
import 'package:daily_score_mobile/router/router.dart' as MyRouter;
import 'package:fluttertoast/fluttertoast.dart';

/// 成绩查询页面
/// @author: dingwen
/// @date: 2021/4/27
class QueryScorePage extends StatefulWidget {
  @override
  _QueryScorePageState createState() => _QueryScorePageState();
}

class _QueryScorePageState extends State<QueryScorePage> {
  TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('成绩查询'),
        centerTitle: true,
        actions: [SimpleDropMenuWidget()],
      ),
      body: ListView(
        children: [
          _buildImage(),
          Center(
            child: Text(
              '大学计算机课程日常成绩管理系统提供成绩查询',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          _buildTextFieldBox(),
          _buildQueryButton(),
        ],
      ),
    );
  }

  /// 构建背景图片
  Widget _buildImage() {
    return Container(
        width: MediaQuery.of(context).size.width / 3,
        height: 160,
        child: Image.network(
            "https://img1.baidu.com/it/u=3221384366,216150308&fm=26&fmt=auto&gp=0.jpg"));
  }

  /// 构建输入框
  Widget _buildTextFieldBox() {
    return Container(
      margin: EdgeInsets.all(30.0),
      color: Colors.yellow[50],
      child: TextField(
        controller: _controller,
        keyboardType: TextInputType.number,
        textAlign: TextAlign.center,
        decoration: InputDecoration(
          hintText: "请输入学号",
          border: InputBorder.none,
        ),
      ),
    );
  }

  /// 构建按钮
  Widget _buildQueryButton() {
    return Container(
      margin: EdgeInsets.only(left: 30.0, right: 30.0),
      child: SignInButton(
        child: Text(
          '查询',
          style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
        ),
        onPressed: () {
          if (_controller.text == "") {
            Fluttertoast.showToast(
              msg: "请输入学号",
            );
          } else {
            Bundle bundle = Bundle();
            bundle.putString('studentId', _controller.text);
            MyRouter.Router.pageTo(score_detail_page,
                routeSettings: RouteSettings(arguments: bundle));
          }
        },
      ),
    );
  }
}
