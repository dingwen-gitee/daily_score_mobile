import 'package:daily_score_mobile/dao/score_dao.dart';
import 'package:daily_score_mobile/model/score_model.dart';
import 'package:daily_score_mobile/page/common/not_data_page.dart';
import 'package:daily_score_mobile/router/bundle.dart';
import 'package:daily_score_mobile/widget/card/card_widget.dart';
import 'package:daily_score_mobile/widget/loading/loading_widget.dart';
import 'package:flutter/material.dart';

/// 成绩详情页
/// @author: dingwen
/// @date: 2021/4/27
class ScoreDetailPage extends StatefulWidget {
  final Bundle bundle;

  ScoreDetailPage(this.bundle);

  @override
  _ScoreDetailPageState createState() => _ScoreDetailPageState();
}

class _ScoreDetailPageState extends State<ScoreDetailPage> {
  // 成信息集合
  List<ScoreModel> scoreModelList = [];
  // 是否已经加载完毕
  bool _isLoading = true;
  @override
  void initState() {
    super.initState();
    fetchData();
  }

  /// 查询成绩
  Future<void> fetchData() async{
    Map params = {
      "pageNum": 1,
      "pageSize": 100000,
      "studentId": widget.bundle?.getString("studentId")
    };
    scoreModelList = await ScoreDao.getScoreList(data: params);

    setState(() {
      _isLoading = false;
    });
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('成绩详情'),
        centerTitle: true,
      ),
      body: scoreModelList.length == 0 ? NotDataPage() : LoadingWidget(
        isLoading: _isLoading,
        child: ListView.builder(
            itemCount: scoreModelList?.length,
            itemBuilder: (BuildContext context, int index) {
              return CardWidget(
                scoreModel: scoreModelList[index],
              );
            }),
      ),
    );
  }
}
