import 'package:flutter/material.dart';
import 'package:daily_score_mobile/router/router.dart' as MyRouter;
import 'package:fluttertoast/fluttertoast.dart';

/// root page App 生命周期
/// App 初始化
///Created by dingwen on 2021/4/12.

class RootPage extends StatefulWidget {
  // 全局路由Key
  final GlobalKey<NavigatorState> globalRouteKey;

  RootPage({this.globalRouteKey}) : assert(globalRouteKey != null);

  @override
  _RootPageState createState() => _RootPageState();
}

/// WidgetsBindingObserver 监听App生命周期
class _RootPageState extends State<RootPage> with WidgetsBindingObserver {
  @override
  void initState() {
    super.initState();
    // 注册生命周期监听
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: widget.globalRouteKey,
      theme: ThemeData(
        primaryColor: Colors.yellow,
        accentColor: Colors.amber,
        highlightColor: Color.fromRGBO(255, 255, 255, 0.6),
        // 水波纹颜色
        splashColor: Colors.yellow,
        fontFamily: 'Roboto',
      ),
      //是否取消右上角的DEBUG标签
      debugShowCheckedModeBanner: false,
      onGenerateRoute: MyRouter.Router.fluroRouter.generator,
      //是否显示性能调试
      showPerformanceOverlay: false,
        builder: (context, widget) {
          return MediaQuery(
            //设置文字大小比例
            data: MediaQuery.of(context).copyWith(textScaleFactor: 0.9),
            child: widget,
          );
        },
    );
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      // 应用程序处于非活动状态，没有接收用户输入
      case AppLifecycleState.inactive:
        break;
      //用程序仍然驻留在Flutter引擎上，但是与任何主机视图分离
      case AppLifecycleState.detached:
        break;
      // 应用程序当前对用户不可见，不响应用户输入，并在后台运行
      case AppLifecycleState.paused:
        Fluttertoast.showToast(
          msg: "平时成绩应用，切换到后台，请注意!",
        );
        break;
      case AppLifecycleState.resumed:
        break;
    }
  }

/// 释放资源
  @override
  void dispose() {
    super.dispose();
    //移除监听器
    WidgetsBinding.instance.removeObserver(this);
  }
}
