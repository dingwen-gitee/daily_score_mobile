import 'package:daily_score_mobile/router/bundle.dart';
import 'package:flutter/material.dart ';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

/// 调转网页承载
///Created by dingwen on 2021/4/13.

class WebSitePage extends StatefulWidget {
  final Bundle bundle;

  WebSitePage(this.bundle);

  @override
  _WebSitePageState createState() => _WebSitePageState();
}

class _WebSitePageState extends State<WebSitePage> {
  @override
  Widget build(BuildContext context) {
    return WebView(
      initialUrl: widget.bundle.getString('url'),
      //是否支持js 默认是不支持的，
      javascriptMode: JavascriptMode.unrestricted,
    );
  }
}
