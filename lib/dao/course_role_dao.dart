import 'package:daily_score_mobile/constant/api_constant.dart';
import 'package:daily_score_mobile/model/course_role_model.dart';
import 'package:daily_score_mobile/util/params_util.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:async';
import 'dart:convert';

/// 课程规则 数据访问
/// @author: dingwen
/// @date: 2021/5/1
class CourseRoleDao {
  /// 查询课程规则信息列表
  static Future<List<CourseRoleModel>> getCourseRoleList({Map data}) async {
    final response = await http.get(
        Uri.parse(course_role_list_uri + ParamsUtil.mapToUri(data)),
        headers: {"content-type": "application/json"});
    Utf8Decoder utf8decoder = Utf8Decoder();
    var result = json.decode(utf8decoder.convert(response.bodyBytes));
    if (result['code'] == 200) {
      List<CourseRoleModel> courseRoleModelList = [];
      List dataList = result['data'];
      dataList.forEach((data) {
        CourseRoleModel courseRoleModel = CourseRoleModel.fromJson(data);
        courseRoleModelList.add(courseRoleModel);
      });
      return courseRoleModelList;
    } else {
      Fluttertoast.showToast(
        msg: "课程规则信息加载失败",
      );
      return null;
    }
  }
}
