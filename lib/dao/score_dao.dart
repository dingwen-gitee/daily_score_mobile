import 'dart:async';
import 'package:daily_score_mobile/constant/api_constant.dart';
import 'package:daily_score_mobile/model/score_model.dart';
import 'package:daily_score_mobile/util/params_util.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:async';
import 'dart:convert';

/// 成绩数据查询
/// @author: dingwen
/// @date: 2021/5/2
class ScoreDao {
  /// 根据学号查询学生成绩
  static Future<List<ScoreModel>> getScoreList({Map data}) async {
    final response = await http.get(
        Uri.parse(score_list_uri + ParamsUtil.mapToUri(data)),
        headers: {"content-type": "application/json"});
    Utf8Decoder utf8decoder = Utf8Decoder();
    var result = json.decode(utf8decoder.convert(response.bodyBytes));
    if (result['code'] == 200) {
      List<ScoreModel> scoreModelList = [];
      List dataList = result['data'];
      dataList.forEach((data) {
        ScoreModel scoreModel = ScoreModel.fromJson(data);
        scoreModelList.add(scoreModel);
      });
      return scoreModelList;
    } else {
      Fluttertoast.showToast(
        msg: "课程规则信息加载失败",
      );
      return null;
    }
  }
}
