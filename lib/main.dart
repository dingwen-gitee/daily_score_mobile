import 'package:daily_score_mobile/page/root_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:io';
import 'package:daily_score_mobile/router/router.dart' as MyRouter;

/// 全局路由KEY
final GlobalKey<NavigatorState> globalRouteKey = GlobalKey<NavigatorState>();

void main() {
  init();
  runApp(RootPage(
    globalRouteKey: globalRouteKey,
  ),);
}

/// 应用初始化
void init() {
  initRouter();
  initCustomErrorPage();
  initSystemUIOverlayStyle();
}

///初始化路由
void initRouter() {
  MyRouter.Router.setUpRoutes();
  MyRouter.Router.navigatorKey = globalRouteKey;
}

///初始化错误错误页面
///[ErrorWidget]Flutter提供
void initCustomErrorPage() {
  ErrorWidget.builder = (FlutterErrorDetails flutterErrorDetails) {
    return ErrorWidget(flutterErrorDetails);
  };
}

///初始化状态栏
void initSystemUIOverlayStyle() {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Colors.transparent,
    statusBarIconBrightness: Brightness.dark,
    statusBarBrightness:
        Platform.isAndroid ? Brightness.dark : Brightness.light,
    systemNavigationBarColor: Colors.white,
    systemNavigationBarDividerColor: Colors.grey,
    systemNavigationBarIconBrightness: Brightness.dark,
  ));
}
