/// api
/// @author: dingwen
/// @date: 2021/5/1
const String base_uri = 'http://192.168.1.108:9999/daily_score_management';
const String login_uri = '$base_uri/student/login';
const String student_info_uri = '$base_uri/student/info/page';
const String course_role_list_uri = '$base_uri/rule/info/page';
const String score_list_uri = '$base_uri/score/info/page';
