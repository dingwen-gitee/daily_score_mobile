/// router constant
///Created by dingwen on 2021/4/12.

const String root = '/';
const String index_page = '/index';
const String splash_page = '/splash';
const String login_page = '/login';
const String home_page = '/home';
const String curved_navigation_bar_page = '/curved';
const String web_site_page = '/website';
const String chat_page = '/chat';
const String course_page = '/course';
const String send_message_page = '/send';
const String notice_page = '/notice';
const String query_page = '/query';
const String score_detail_page = '/detail';
