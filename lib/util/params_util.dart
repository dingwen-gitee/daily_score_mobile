/// 参数处理小工具
/// @author: dingwen
/// @date: 2021/5/1
class ParamsUtil {
  /// map to uri
  static String mapToUri(Map map) {
    int index = 0;
    String uri = '';
    map.forEach((key, value) {
      if (index == 0) {
        uri += "?" + '$key' + "=" + '$value';
      } else if (index != map.length) {
        uri += "&" + '$key' + "=" + '$value';
      } else {
        uri += '$key' + "=" + '$value';
      }
      index++;
    });
    return uri;
  }
}
