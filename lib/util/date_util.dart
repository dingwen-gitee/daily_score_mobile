/// date 工具类
/// @author: dingwen
/// @date: 2021/4/15
import 'package:intl/intl.dart';

class DateUtil {
  static String getCurrentTime() {
    DateTime now = DateTime.now();
    var formatter = DateFormat('yyyy-MM-dd hh:mm:ss');
    return formatter.format(now);
  }
}