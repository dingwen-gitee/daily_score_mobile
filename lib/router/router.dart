import 'package:daily_score_mobile/constant/router_constant.dart';
import 'package:daily_score_mobile/page/common/curved_navigation_bar_page.dart';
import 'package:daily_score_mobile/page/common/not_find_page.dart';
import 'package:daily_score_mobile/page/common/splash_page.dart';
import 'package:daily_score_mobile/page/home/home_page.dart';
import 'package:daily_score_mobile/page/home/page/course_role_page.dart';
import 'package:daily_score_mobile/page/index_page.dart';
import 'package:daily_score_mobile/page/login/login_page.dart';
import 'package:daily_score_mobile/page/message/page/chat_page.dart';
import 'package:daily_score_mobile/page/message/page/notice_page.dart';
import 'package:daily_score_mobile/page/message/page/send_message_page.dart';
import 'package:daily_score_mobile/page/score/page/query_score_page.dart';
import 'package:daily_score_mobile/page/score/page/score_detail_page.dart';
import 'package:daily_score_mobile/page/website/website_page.dart';
import 'package:daily_score_mobile/router/page_builder.dart';
import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';


/// 基于fluro路由管理
/// getHandler 最终返回fluro所需要的handler
///Created by dingwen on 2021/4/12.

class Router {
  // 初始化路由
  static final fluroRouter = FluroRouter();

  // 全局路由Key 针对没有上下文页面跳转的情况
  static GlobalKey<NavigatorState> navigatorKey =
      new GlobalKey<NavigatorState>();

  /// 路由、页面集合
  static final Map<String, PageBuilder> pageRoutes = {
    root: PageBuilder(builder: (bundle) => IndexPage()),
    splash_page: PageBuilder(builder: (bundle) => SplashPage()),
    login_page: PageBuilder(builder: (bundle) => LoginPage()),
    home_page: PageBuilder(builder: (bundle) => HomePage()),
    chat_page: PageBuilder(builder: (bundle) => ChatPage()),
    course_page: PageBuilder(builder: (bundle) => CoursePage()),
    send_message_page: PageBuilder(builder: (bundle) => SendMessagePage()),
    notice_page: PageBuilder(builder: (bundle) => NoticePage()),
    query_page: PageBuilder(builder: (bundle) => QueryScorePage()),
    score_detail_page: PageBuilder(builder: (bundle) => ScoreDetailPage(bundle)),
    web_site_page: PageBuilder(builder: (bundle) => WebSitePage(bundle)),
    curved_navigation_bar_page:
        PageBuilder(builder: (bundle) => CuredNavigationBarPage()),
  };

  /// 路由装配
  static setUpRoutes() {
    pageRoutes.forEach((path, handler) {
      fluroRouter.define(path, handler: handler.getHandler());
    });
    // 404页面设置
    fluroRouter.notFoundHandler = Handler(handlerFunc: (context, params) {
      return NotFindPage();
    });
  }

  ///页面跳转
  ///路由动画
  ///时间
  static Future<dynamic> pageTo(String path,
      {TransitionType transitionType = TransitionType.fadeIn,
      BuildContext context,
      RouteSettings routeSettings}) {
    return fluroRouter.navigateTo(context ?? navigatorKey.currentContext, path,
        routeSettings: routeSettings,
        transition: transitionType,
        transitionDuration: Duration(milliseconds: 250));
  }
}
