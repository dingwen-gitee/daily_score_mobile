import 'package:daily_score_mobile/router/bundle.dart';
import 'package:flutter/material.dart';
import 'package:fluro/fluro.dart';

/// page_builder
/// getHandler 最终返回fluro所需要的handler
///Created by dingwen on 2021/4/12.

typedef Widget HandlerFunc(BuildContext context,Map<String,List<String>> parameters);
typedef Widget PageBuilderFunc(Bundle bundle);

class PageBuilder{
  final PageBuilderFunc builder;
  HandlerFunc handlerFunc;
  PageBuilder({this.builder}){
    this.handlerFunc = (context,parameters){
      return this.builder(ModalRoute.of(context).settings.arguments as Bundle);
    };
  }

  ///最终返回fluro所需要的handler
  Handler getHandler()=> Handler(handlerFunc: this.handlerFunc);
}