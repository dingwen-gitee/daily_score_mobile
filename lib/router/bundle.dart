import 'package:flutter/cupertino.dart';

/// 路由传参捆绑类
/// 约束数据类型
/// 简洁的路由管理
///Created by dingwen on 2021/4/12.
class Bundle {
  ///传参数据集合
  Map<String, dynamic> _map = {};

  ///设置参数
  _setValue(var k, var v) => _map[k] = v;

  ///获取参数
  _getValue(String k) {
    if (!_map.containsKey(k)) {
      print("路由参数key不存在");
      return;
    }
    return _map[k];
  }

  putInt(String k, int v) => _setValue(k, v);

  putDouble(String k, double v) => _setValue(k, v);

  putString(String k, String v) => _setValue(k, v);

  putBool(String k, bool v) => _setValue(k, v);

  putList<V>(String k, List<V> v) => _setValue(k, v);

  putMap<K, V>(String k, Map<K, V> v) => _setValue(k, v);

  int getInt(String k) => _getValue(k) as int;

  double getDouble(String k) => _getValue(k) as double;

  String getString(String k) => _getValue(k) as String;

  bool getBool(String k) => _getValue(k) as bool;

  List getList(String k) => _getValue(k) as List;

  Map getMap(String k) => _getValue(k) as Map;

  @override
  String toString() {
    return _map.toString();
  }
}
