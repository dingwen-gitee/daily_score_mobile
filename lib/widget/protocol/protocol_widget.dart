import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

/// 用户协议
/// @author: dingwen
/// @date: 2021/1/2

class ProtocolWidget {
  ///协议内容
  static const String _protocolStr =
      '我们一向尊重并会严格保护用户在使用本产品时的合法权益（包括用户隐私、用户数据等）不受到任何侵犯。本协议（包括本文最后部分的隐私政策）是用户（包括通过各种合法途径获取到本产品的自然人、法人或其他组织机构，以下简称“用户”或“您”）'
      '与我们之间针对本产品相关事项最终的、完整的且排他的协议，并取代、合并之前的当事人之间关于上述事项的讨论和协议。本协议将对用户使用本产品的行为产生法律约束力，'
      '您已承诺和保证有权利和能力订立本协议。用户开始使用本产品将视为已经接受本协议，请认真阅读并理解本协议中各种条款，包括免除和限制我们的免责条款和对用户的权利限制'
      '（未成年人审阅时应由法定监护人陪同），如果您不能接受本协议中的全部条款，请勿开始使用本产品。';

  ///用户协议手势
  TapGestureRecognizer _userProtocolRecognizer;

  ///隐私协议手势
  TapGestureRecognizer _privateProtocolRecognizer;

  ///构建用户协议对话框
  Future<bool> showUserProtocol(BuildContext context) async {
    //手势初始化
    _userProtocolRecognizer = TapGestureRecognizer();
    _privateProtocolRecognizer = TapGestureRecognizer();

    bool isAgreement = await showCupertinoDialog(
        context: context,
        builder: (BuildContext context) => cupertinoAlertDialog(context));

    //销毁手势
    _userProtocolRecognizer.dispose();
    _privateProtocolRecognizer.dispose();
    return Future.value(isAgreement);
  }

  ///弹出苹果风格的对话框
  cupertinoAlertDialog(BuildContext context) {
    return CupertinoAlertDialog(
      title: Text('温馨提示'),
      content: Container(
        height: 320,
        padding: EdgeInsets.all(12),
        child: SingleChildScrollView(
          child: _buildContent(context),
        ),
      ),
      actions: [
        CupertinoDialogAction(
          child: Text('不同意'),
          onPressed: () => Navigator.of(context).pop(false),
        ),
        CupertinoDialogAction(
          child: Text('同意'),
          onPressed: () => Navigator.of(context).pop(true),
        ),
      ],
    );
  }

  ///构建对话框内容
  _buildContent(BuildContext context) {
    TextStyle styleGrey = TextStyle(color: Colors.grey[600]);
    TextStyle styleBlue = TextStyle(color: Colors.blue);
    return RichText(
        text: TextSpan(text: '请您在使用本产品之前仔细阅读', style: styleGrey, children: [
      TextSpan(
          text: '《用户协议》',
          style: styleBlue,
          //点击事件,打开用户协议
          recognizer: _userProtocolRecognizer
            ..onTap = () => openUserProtocol(context)),
      TextSpan(
        text: '与',
        style: styleGrey,
      ),
      TextSpan(
          text: '《隐私协议》',
          style: styleBlue,
          //点击事件吗，打开隐私协议
          recognizer: _privateProtocolRecognizer
            ..onTap = () => openPrivateProtocol(context)),
      TextSpan(text: '$_protocolStr', style: styleGrey)
    ]));
  }

  ///打开用户协议
  openUserProtocol(BuildContext context) {
    //todo WebView 实现
  }

  ///打开隐私协议
  openPrivateProtocol(BuildContext context) {
    //todo WebView 实现
  }
}
