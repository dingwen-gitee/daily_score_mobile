import 'package:flutter/material.dart';
/// 指示器
/// @author: dingwen
/// @date: 2021/4/25
class IndicatorWidget extends StatelessWidget {
 final Color color;
 final String title;
  IndicatorWidget({this.color,this.title});
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          width: 20,
          height: 20,
          color: color,
          margin: EdgeInsets.all(6.0),
        ),
        Text('$title',style: TextStyle(fontWeight: FontWeight.bold),)
      ],
    );
  }
}
