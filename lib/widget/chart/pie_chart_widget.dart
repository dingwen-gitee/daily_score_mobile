import 'package:daily_score_mobile/model/course_role_model.dart';
import 'package:daily_score_mobile/widget/indicator/indicator_widget.dart';
import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';

/// 饼图
/// @author: dingwen
/// @date: 2021/4/25
class PieChartWidget extends StatefulWidget {

  final CourseRoleModel courseRoleModel;

  PieChartWidget({this.courseRoleModel});


  @override
  _PieChartWidgetState createState() => _PieChartWidgetState();
}

class _PieChartWidgetState extends State<PieChartWidget> {
  int touchedIndex;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery
          .of(context)
          .size
          .height / 2.7,
      child: Card(
        elevation: 5, //阴影
        shape: const RoundedRectangleBorder(
          //形状
          //修改圆角
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
        color: Colors.grey[200],

        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                    margin: EdgeInsets.only(left: 20.0, top: 20.0),
                    child: Text(
                      '${widget.courseRoleModel?.courseName}',
                      style: TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.black),
                    )),
                Container(
                    margin: EdgeInsets.only(right: 20.0, top: 20.0),
                    child: Text(
                      '${widget.courseRoleModel?.teacherName}',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )),
              ],
            ),
            Row(
              children: <Widget>[
                const SizedBox(
                  height: 18,
                ),
                Expanded(
                  child: AspectRatio(
                    aspectRatio: 1,
                    child: PieChart(
                      PieChartData(
                          pieTouchData:
                          PieTouchData(touchCallback: (pieTouchResponse) {
                            setState(() {});
                          }),
                          borderData: FlBorderData(
                            show: false,
                          ),
                          sectionsSpace: 0,
                          centerSpaceRadius: 40,
                          sections: showingSections()),
                    ),
                  ),
                ),
                Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    IndicatorWidget(
                      color: Color(0xff0293ee),
                      title: '考勤',
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    IndicatorWidget(
                      color: Color(0xfff8b250),
                      title: '作业',
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    IndicatorWidget(
                      color: Color(0xff845bef),
                      title: '上课',
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    IndicatorWidget(
                      color: Color(0xff13d38e),
                      title: '期中',
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    IndicatorWidget(
                      color: Colors.red,
                      title: '期末',
                    ),
                    SizedBox(
                      height: 18,
                    ),
                  ],
                ),
                const SizedBox(
                  width: 28,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  List<PieChartSectionData> showingSections() {
    return List.generate(5, (i) {
      final isTouched = i == touchedIndex;
      final double fontSize = isTouched ? 25 : 16;
      final double radius = isTouched ? 60 : 50;
      switch (i) {
        case 0:
          // 考勤
          return PieChartSectionData(
            color: const Color(0xff0293ee),
            value: widget.courseRoleModel.ruleAttendance * 100 ?? 0,
            title: '${widget.courseRoleModel.ruleAttendance * 100}%',
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize,
                fontWeight: FontWeight.bold,
                color: const Color(0xffffffff)),
          );
        case 1:
          // 作业
          return PieChartSectionData(
            color: const Color(0xfff8b250),
            value: widget.courseRoleModel.ruleHomework * 100 ?? 0,
            title: '${widget.courseRoleModel.ruleHomework * 100}%',
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize,
                fontWeight: FontWeight.bold,
                color: const Color(0xffffffff)),
          );
        case 2:
          // 上课
          return PieChartSectionData(
            color: const Color(0xff845bef),
            value: widget.courseRoleModel.ruleClazz * 100 ?? 0,
            title: '${widget.courseRoleModel.ruleClazz * 100}%',
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize,
                fontWeight: FontWeight.bold,
                color: const Color(0xffffffff)),
          );

        case 3:
          // 期中
          return PieChartSectionData(
            color: const Color(0xff13d38e),
            value: widget.courseRoleModel.ruleMiddle * 100 ?? 0,
            title: '${widget.courseRoleModel.ruleMiddle * 100}%',
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize,
                fontWeight: FontWeight.bold,
                color: const Color(0xffffffff)),
          );
        case 4:
          //期末
          return PieChartSectionData(
            color: Colors.red,
            value: widget.courseRoleModel.ruleEnd * 100 ?? 0,
            title: '${widget.courseRoleModel.ruleEnd * 100}%',
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize,
                fontWeight: FontWeight.bold,
                color: const Color(0xffffffff)),
          );
        default:
          return null;
      }
    });
  }
}
