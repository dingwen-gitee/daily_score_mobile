import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';

/// 动画效果的底部导航栏
/// @author: dingwen
/// @date: 2021/1/9

// ignore: must_be_immutable
class CurvedNavigationBarWidget extends StatefulWidget {
  ///需要跳转的页面
  final List<Widget> pages;

  ///底部导航图标和图标对应标题
  final Map<String, IconData> icons;

  ///图标大小
  final double iconSize;

  ///标题文字大小
  final double titleFontSize;

  ///激活颜色
  MaterialColor activeColor;

  ///失活颜色
  MaterialColor inactiveColor;

  ///底部导航栏高度最高 75
  final double barHeight;

  CurvedNavigationBarWidget(
      {Key key,
      this.pages,
      this.icons,
      this.iconSize = 30.0,
      this.titleFontSize = 4.0,
      this.activeColor,
      this.inactiveColor,
      this.barHeight = 42.0})
      : assert(barHeight < 75),
        assert(pages != null && icons != null),
        assert(pages.length == icons.length),
        super(key: key);

  @override
  _CurvedNavigationBarWidgetState createState() =>
      _CurvedNavigationBarWidgetState();
}

class _CurvedNavigationBarWidgetState extends State<CurvedNavigationBarWidget> {
  ///当前激活的下标
  int _index = 2;

  ///PageView 控制器
  ///开启页面缓存
  PageController _pageController =
      PageController(initialPage: 2, keepPage: true);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: CurvedNavigationBar(
          index: 2,
          height: widget.barHeight,
          items: _buildIconList(context),
          color: Colors.white,
          buttonBackgroundColor: Colors.white,
          backgroundColor: Color.fromRGBO(236, 240, 241, 1),
          animationCurve: Curves.easeIn,
          animationDuration: Duration(milliseconds: 600),
          onTap: (index) {
            setState(() {
              _index = index;
              _pageController.animateToPage(index,
                  duration: Duration(milliseconds: 300), curve: Curves.easeIn);
            });
          },
          letIndexChange: (index) => true,
        ),
        body: PageView.builder(
            physics: NeverScrollableScrollPhysics(),
            //禁止滑动
            controller: _pageController,
            itemCount: widget.pages.length,
            onPageChanged: (index) => setState(() {
                  _index = index;
                }),
            itemBuilder: (context, index) => widget.pages[index]));
  }

  ///构建图标
  List<Widget> _buildIconList(context) {
    List<Widget> _iconList = [];
    //Map 下标
    int index = 0;
    widget.icons.forEach((title, iconData) {
      _iconList.add(Tooltip(
        message: '$title',
        child: Container(
          child: Column(
          children: [
            Icon(iconData,
                size: widget.iconSize,
                color: _index == index
                    ? widget.activeColor ?? Theme.of(context).accentColor
                    : widget.inactiveColor ?? Colors.black38),
            Text('$title',
                style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: 12.0,
                  color: _index == index
                      ? widget.activeColor ?? Theme.of(context).accentColor
                      : widget.inactiveColor ?? Color(0xFF213333),
                )),
          ],
        ),),
      ));
      index++;
    });
    return _iconList;
  }
}
