import 'package:daily_score_mobile/model/score_model.dart';
import 'package:flutter/material.dart';

/// 成绩 widget
/// @author: dingwen
/// @date: 2021/4/27

class CardWidget extends StatefulWidget {
  final ScoreModel scoreModel;

  CardWidget({this.scoreModel});

  @override
  _CardWidgetState createState() => _CardWidgetState();
}

class _CardWidgetState extends State<CardWidget> {
  @override
  Widget build(BuildContext context) {
    return Card(
        //z轴的高度，设置card的阴影
        elevation: 5.0,
        //设置shape，这里设置成了R角
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(5.0)),
        ),
        //对Widget截取的行为，比如这里 Clip.antiAlias 指抗锯齿
        clipBehavior: Clip.antiAlias,
        semanticContainer: false,
        margin: EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 5.0, 5.0, 0),
                  child: ListTile(
                    leading: Icon(
                      Icons.child_care,
                      size: 30,
                      color: Colors.deepOrange,
                    ),
                    title: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Container(
                        child: Text(
                          "${widget?.scoreModel?.courseName}",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                              fontSize: 18),
                        ),
                      ),
                    ),
                    trailing: Text(
                      "${widget?.scoreModel?.teacherName}",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                          fontSize: 18),
                    ),
                  ),
                )),
            Divider(
              color: Colors.grey,
            ),
            ListTile(
              leading: Text(
                "姓名:",
                style: TextStyle(
                    color: Colors.black54, letterSpacing: 1, fontSize: 18),
              ),
              title: Container(
                margin: EdgeInsets.only(bottom: 4.0),
                child: Text("${widget?.scoreModel?.studentName}",
                    style: TextStyle(
                        color: Colors.black, letterSpacing: 1, fontSize: 18)),
              ),
              trailing:
                  Text('${widget?.scoreModel?.clazzYear}级${widget?.scoreModel?.clazzName}', style: TextStyle(color: Colors.green)),
            ),
            ListTile(
              leading: Text(
                "考勤成绩:",
                style: TextStyle(
                    color: Colors.black54, letterSpacing: 1, fontSize: 18),
              ),
              title: Text("${widget?.scoreModel?.scoreAttendance}",
                  style: TextStyle(
                      color: Colors.black, letterSpacing: 1, fontSize: 18)),
              trailing: getScoreLevel(widget?.scoreModel?.scoreAttendance ?? 0),
            ),
            ListTile(
              leading: Text(
                '作业成绩:',
                style: TextStyle(
                    color: Colors.black54, letterSpacing: 1, fontSize: 18),
              ),
              title: Text(
                "${widget?.scoreModel?.scoreHomework}",
                style: TextStyle(
                    color: Colors.black, letterSpacing: 1, fontSize: 18),
              ),
              trailing: getScoreLevel(widget?.scoreModel?.scoreHomework ?? 0),
            ),
            ListTile(
              leading: Text(
                "上课成绩:",
                style: TextStyle(
                    color: Colors.black54, letterSpacing: 1, fontSize: 18),
              ),
              title: Text("${widget?.scoreModel?.scoreClazz}",
                  style: TextStyle(
                      color: Colors.black, letterSpacing: 1, fontSize: 18)),
              trailing: getScoreLevel(widget?.scoreModel?.scoreClazz ?? 0),
            ),
            ListTile(
              leading: Text(
                "期中成绩:",
                style: TextStyle(
                    color: Colors.black54, letterSpacing: 1, fontSize: 18),
              ),
              title: Text("${widget?.scoreModel?.scoreMiddle}",
                  style: TextStyle(
                      color: Colors.black, letterSpacing: 1, fontSize: 18)),
              trailing: getScoreLevel(widget?.scoreModel?.scoreMiddle ?? 0),
            ),
            ListTile(
              leading: Text(
                "期末成绩:",
                style: TextStyle(
                    color: Colors.black54, letterSpacing: 1, fontSize: 18),
              ),
              title: Text("${widget?.scoreModel?.scoreEnd}",
                  style: TextStyle(
                      color: Colors.black, letterSpacing: 1, fontSize: 18)),
              trailing: getScoreLevel(widget?.scoreModel?.scoreEnd ?? 0),
            ),
            ListTile(
                leading: Text(
                  "最终成绩:",
                  style: TextStyle(
                      color: Colors.black54, letterSpacing: 1, fontSize: 18),
                ),
                title: Text("${widget?.scoreModel?.scoreFinal}",
                    style: TextStyle(
                        color: Colors.red,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1,
                        fontSize: 18)),
              trailing: getScoreLevel(widget?.scoreModel?.scoreFinal ?? 0)),
          ],
        ));
  }


  /// 获取成绩水平层次
  Text getScoreLevel(double score){
    if (score < 60) {
      return Text('差',style: TextStyle(color: Colors.red),);
    } else if (score < 70 ){
      return Text('中',style: TextStyle(color: Colors.yellow),);
    } else if (score < 90) {
      return Text('良',style: TextStyle(color: Colors.blue),);
    } else{
      return Text('优',style: TextStyle(color: Colors.green),);
    }
  }
}
