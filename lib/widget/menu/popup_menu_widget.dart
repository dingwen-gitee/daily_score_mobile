import 'package:flutter/material.dart';

/// popup 菜单
/// @author: dingwen
/// @date: 2021/4/15
class PopupMenuWidget extends StatefulWidget {

  final Map<String, Widget> popupMenuMap;

  PopupMenuWidget({this.popupMenuMap});

  @override
  _PopupMenuWidgetState createState() => _PopupMenuWidgetState();
}

class _PopupMenuWidgetState extends State<PopupMenuWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 10.0),
      child: _buildMenu(),
    );
  }


  /// 构建菜单
  Widget _buildMenu() {
    return PopupMenuButton<dynamic>(
      color: Colors.black,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      // color: Colors.black,
      itemBuilder: (context) {
        return _buildMenuItem();
      },
    );
  }

  /// 构建菜单Item
  List<PopupMenuItem<dynamic>> _buildMenuItem() {
    List<PopupMenuItem> widgetList = [];
    widget.popupMenuMap?.forEach((val, widget) {
      widgetList.add(PopupMenuItem<String>(
        value: '$val',
        child: widget,
      ),);
    });
    return widgetList;
  }
}
