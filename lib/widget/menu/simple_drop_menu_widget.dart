import 'package:flutter/material.dart';

/// 简单下拉菜单
///Created by dingwen on 2021/4/13.

class SimpleDropMenuWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return IconButton(
        icon: Icon(Icons.more_horiz),
        onPressed: () => {
              showModalBottomSheet(
                  context: context,
                  builder: (BuildContext context) {
                    return Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        ListTile(
                          title: Center(
                            child: Text('刷新'),
                          ),
                            onTap: () async => {Navigator.of(context).pop()},
                        ),
                        Divider(),
                        ListTile(
                          title: Center(
                            child: Text('取消'),
                          ),
                          onTap: () async => {Navigator.of(context).pop()},
                        ),
                      ],
                    );
                  })
            });
  }
}
