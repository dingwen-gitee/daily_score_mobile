/// 学生实体
/// @author: dingwen
/// @date: 2021/5/1
class StudentModel {
  String clazzId;
  int studentGender;
  String studentId;
  String studentImg;
  String studentName;
  String studentPassword;
  String studentQq;
  String studentWechat;

  StudentModel(
      {this.clazzId,
      this.studentGender,
      this.studentId,
      this.studentImg,
      this.studentName,
      this.studentPassword,
      this.studentQq,
      this.studentWechat});

  StudentModel.fromJson(Map<String, dynamic> json) {
    clazzId = json['clazzId'];
    studentGender = json['studentGender'];
    studentId = json['studentId'];
    studentImg = json['studentImg'];
    studentName = json['studentName'];
    studentPassword = json['studentPassword'];
    studentQq = json['studentQq'];
    studentWechat = json['studentWechat'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['clazzId'] = this.clazzId;
    data['studentGender'] = this.studentGender;
    data['studentId'] = this.studentId;
    data['studentImg'] = this.studentImg;
    data['studentName'] = this.studentName;
    data['studentPassword'] = this.studentPassword;
    data['studentQq'] = this.studentQq;
    data['studentWechat'] = this.studentWechat;
    return data;
  }
}
