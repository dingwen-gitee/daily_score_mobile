/// 课程模型
/// @author: dingwen
/// @date: 2021/5/1
class CourseRoleModel {
  String courseId;
  String courseName;
  double ruleAttendance;
  double ruleClazz;
  double ruleEnd;
  double ruleHomework;
  double ruleMiddle;
  String teacherId;
  String teacherName;

  CourseRoleModel(
      {this.courseId,
      this.courseName,
      this.ruleAttendance,
      this.ruleClazz,
      this.ruleEnd,
      this.ruleHomework,
      this.ruleMiddle,
      this.teacherId,
      this.teacherName});

  CourseRoleModel.fromJson(Map<String, dynamic> json) {
    courseId = json['courseId'];
    courseName = json['courseName'];
    ruleAttendance = json['ruleAttendance'];
    ruleClazz = json['ruleClazz'];
    ruleEnd = json['ruleEnd'];
    ruleHomework = json['ruleHomework'];
    ruleMiddle = json['ruleMiddle'];
    teacherId = json['teacherId'];
    teacherName = json['teacherName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['courseId'] = this.courseId;
    data['courseName'] = this.courseName;
    data['ruleAttendance'] = this.ruleAttendance;
    data['ruleClazz'] = this.ruleClazz;
    data['ruleEnd'] = this.ruleEnd;
    data['ruleHomework'] = this.ruleHomework;
    data['ruleMiddle'] = this.ruleMiddle;
    data['teacherId'] = this.teacherId;
    data['teacherName'] = this.teacherName;
    return data;
  }
}
