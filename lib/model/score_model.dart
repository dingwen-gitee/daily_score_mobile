/// 成绩模型
/// @author: dingwen
/// @date: 2021/5/2
class ScoreModel {
  String clazzId;
  String clazzName;
  String clazzYear;
  String courseId;
  String courseName;
  int courseType;
  String courseTypeStr;
  double scoreAttendance;
  double scoreClazz;
  double scoreEnd;
  double scoreFinal;
  double scoreHomework;
  String scoreId;
  double scoreMiddle;
  String studentId;
  String studentName;
  String teacherId;
  String teacherName;

  ScoreModel(
      {this.clazzId,
        this.clazzName,
        this.clazzYear,
        this.courseId,
        this.courseName,
        this.courseType,
        this.courseTypeStr,
        this.scoreAttendance,
        this.scoreClazz,
        this.scoreEnd,
        this.scoreFinal,
        this.scoreHomework,
        this.scoreId,
        this.scoreMiddle,
        this.studentId,
        this.studentName,
        this.teacherId,
        this.teacherName});

  ScoreModel.fromJson(Map<String, dynamic> json) {
    clazzId = json['clazzId'];
    clazzName = json['clazzName'];
    clazzYear = json['clazzYear'];
    courseId = json['courseId'];
    courseName = json['courseName'];
    courseType = json['courseType'];
    courseTypeStr = json['courseTypeStr'];
    scoreAttendance = json['scoreAttendance'];
    scoreClazz = json['scoreClazz'];
    scoreEnd = json['scoreEnd'];
    scoreFinal = json['scoreFinal'];
    scoreHomework = json['scoreHomework'];
    scoreId = json['scoreId'];
    scoreMiddle = json['scoreMiddle'];
    studentId = json['studentId'];
    studentName = json['studentName'];
    teacherId = json['teacherId'];
    teacherName = json['teacherName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['clazzId'] = this.clazzId;
    data['clazzName'] = this.clazzName;
    data['clazzYear'] = this.clazzYear;
    data['courseId'] = this.courseId;
    data['courseName'] = this.courseName;
    data['courseType'] = this.courseType;
    data['courseTypeStr'] = this.courseTypeStr;
    data['scoreAttendance'] = this.scoreAttendance;
    data['scoreClazz'] = this.scoreClazz;
    data['scoreEnd'] = this.scoreEnd;
    data['scoreFinal'] = this.scoreFinal;
    data['scoreHomework'] = this.scoreHomework;
    data['scoreId'] = this.scoreId;
    data['scoreMiddle'] = this.scoreMiddle;
    data['studentId'] = this.studentId;
    data['studentName'] = this.studentName;
    data['teacherId'] = this.teacherId;
    data['teacherName'] = this.teacherName;
    return data;
  }
}
