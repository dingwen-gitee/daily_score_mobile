/// 学生信息 model
/// @author: dingwen
/// @date: 2021/5/1
class StudentInfoModel {
  String clazzId;
  String clazzName;
  String clazzYear;
  int studentGender;
  String studentId;
  String studentImg;
  String studentName;
  String studentPassword;
  String studentQq;
  String studentWechat;
  String teacherId;
  String teacherName;
  String teacherWechat;

  StudentInfoModel(
      {this.clazzId,
        this.clazzName,
        this.clazzYear,
        this.studentGender,
        this.studentId,
        this.studentImg,
        this.studentName,
        this.studentPassword,
        this.studentQq,
        this.studentWechat,
        this.teacherId,
        this.teacherName,
        this.teacherWechat});

  StudentInfoModel.fromJson(Map<String, dynamic> json) {
    clazzId = json['clazzId'];
    clazzName = json['clazzName'];
    clazzYear = json['clazzYear'];
    studentGender = json['studentGender'];
    studentId = json['studentId'];
    studentImg = json['studentImg'];
    studentName = json['studentName'];
    studentPassword = json['studentPassword'];
    studentQq = json['studentQq'];
    studentWechat = json['studentWechat'];
    teacherId = json['teacherId'];
    teacherName = json['teacherName'];
    teacherWechat = json['teacherWechat'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['clazzId'] = this.clazzId;
    data['clazzName'] = this.clazzName;
    data['clazzYear'] = this.clazzYear;
    data['studentGender'] = this.studentGender;
    data['studentId'] = this.studentId;
    data['studentImg'] = this.studentImg;
    data['studentName'] = this.studentName;
    data['studentPassword'] = this.studentPassword;
    data['studentQq'] = this.studentQq;
    data['studentWechat'] = this.studentWechat;
    data['teacherId'] = this.teacherId;
    data['teacherName'] = this.teacherName;
    data['teacherWechat'] = this.teacherWechat;
    return data;
  }
}